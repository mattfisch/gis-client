package at.fhooe.mc.ois.gisclient.entity.geoobject;

import at.fhooe.mc.ois.gisclient.util.PresentationSchema;
import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;

import java.awt.*;

/**
 * This class represents an area
 */
public class GeoArea implements GeoObjectPart {

    /**
     * The areas geometry
     */
    private final Polygon mGeometry;

    /**
     * Constructor
     *
     * @param _geometry the areas geometry
     */
    public GeoArea(Polygon _geometry) {
        mGeometry = _geometry;
    }

    @Override
    public Rectangle getBounds() {
        return mGeometry.getBounds();
    }

    @Override
    public void draw(Graphics2D _g, Matrix _m, PresentationSchema _schema) {
        _g.setColor(_schema.getLineColor());
        _g.setStroke(new BasicStroke(_schema.getLineWidth()));
        _g.drawPolygon(_m.multiply(mGeometry));
    }

    @Override
    public boolean contains(Point _point) {
        return mGeometry.contains(_point);
    }
}
