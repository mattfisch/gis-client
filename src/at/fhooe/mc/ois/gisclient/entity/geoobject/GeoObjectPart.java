package at.fhooe.mc.ois.gisclient.entity.geoobject;

import at.fhooe.mc.ois.gisclient.util.PresentationSchema;
import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;

import java.awt.*;

/**
 * This interface represents a part, which can be part of a geo object's object collection
 */
public interface GeoObjectPart {

    /**
     * Returns the bounds of the GeoObjectPart
     *
     * @return Bounds as Rectangle
     */
    Rectangle getBounds();

    /**
     * Draws the GeoObjectPart into the supplied graphics2D context
     *
     * @param _g      the graphics2d context into which to draw
     * @param _m      the matrix, which handles scaling, rotation, translation, etc...
     * @param _schema the schema, which handles the coloring of the GeoObjectPart
     */
    void draw(Graphics2D _g, Matrix _m, PresentationSchema _schema);

    /**
     * Checks, if the given point is inside the GeoObjectPart
     *
     * @param _point the point, which will be checked against
     * @return true, if the point is inside the GeoObjectPart, false otherwise
     */
    boolean contains(Point _point);
}
