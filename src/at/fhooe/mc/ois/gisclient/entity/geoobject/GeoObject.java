package at.fhooe.mc.ois.gisclient.entity.geoobject;

import at.fhooe.mc.ois.gisclient.util.DrawingContext;
import at.fhooe.mc.ois.gisclient.util.PresentationSchema;
import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;

import java.awt.*;
import java.util.Arrays;

/**
 * A geo object represents a three dimensional object in the world as a two dimensional polygon, which can be drawn on
 * the screen
 */
public class GeoObject implements IGeoObject {

    /**
     * The id of the geo object
     */
    private String mId;

    /**
     * The type of the geo object
     */
    private int mType;

    /**
     * The polygons used to fill the geo objects geometry parts
     */
    private Polygon[] mFillPloygon;

    /**
     * The polygons, representing the geometry of the geo object
     */
    private GeoObjectPart[] mGeoObjectParts;

    /**
     * The geo objects attributes
     */
    private String mAttributes;

    /**
     * Create a new GeoObject, which cant be drawn yet, since its missing the polygon
     *
     * @param _id    Die Id des Objektes
     * @param _type  Der Typ des Objektes
     * @param _attrs Die Attribute des Objektes
     */
    public GeoObject(String _id, int _type, String _attrs) {
        this(_id, _type, (GeoObjectPart) null, _attrs, null);
    }

    /**
     * Constructor
     *
     * @param _id             Die Id des Objektes
     * @param _type           Der Typ des Objektes
     * @param _geoObjectParts Die Geometrie der Objekte
     */
    public GeoObject(String _id, int _type, GeoObjectPart[] _geoObjectParts) {
        this(_id, _type, _geoObjectParts, "", null);
    }

    /**
     * Constructor
     *
     * @param _id             Die Id des Objektes
     * @param _type           Der Typ des Objektes
     * @param _geoObjectParts Die Geometrie der Objekte
     */
    public GeoObject(String _id, int _type, GeoObjectPart _geoObjectParts) {
        this(_id, _type, _geoObjectParts, "", null);
    }

    /**
     * Constructor
     *
     * @param _id             Die Id des Objektes
     * @param _type           Der Typ des Objektes
     * @param _geoObjectParts Die Geometrie des Objektes
     * @param _attrs          Die Attribute des Objektes
     */
    public GeoObject(String _id, int _type, GeoObjectPart[] _geoObjectParts, String _attrs, Polygon[] _fillPloygon) {
        mId = _id;
        mType = _type;
        mGeoObjectParts = _geoObjectParts;
        mAttributes = _attrs;
        mFillPloygon = _fillPloygon;
    }

    /**
     * Constructor
     *
     * @param _id             Die Id des Objektes
     * @param _type           Der Typ des Objektes
     * @param _geoObjectParts Die Geometrie des Objektes
     * @param _attrs          Die Attribute des Objektes
     */
    public GeoObject(String _id, int _type, GeoObjectPart _geoObjectParts, String _attrs, Polygon _fillPloygon) {
        mId = _id;
        mType = _type;
        mGeoObjectParts = new GeoObjectPart[]{_geoObjectParts};
        mAttributes = _attrs;
        mFillPloygon = new Polygon[]{_fillPloygon};
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public int getType() {
        return mType;
    }

    @Override
    public GeoObjectPart[] getGeoObjectParts() {
        return mGeoObjectParts;
    }

    /**
     * Sets a single polygon as the geo objects polygon data
     *
     * @param _geoObjectParts the polygon, which will later be drawn
     */
    public void setGeoObjectPart(GeoObjectPart _geoObjectParts) {
        mGeoObjectParts = new GeoObjectPart[]{_geoObjectParts};
    }

    /**
     * Sets the geo objects polygons
     *
     * @param _polygons the polygons, which will later be drawn
     */
    public void setGeoObjectParts(GeoObjectPart[] _polygons) {
        mGeoObjectParts = _polygons;
    }

    /**
     * The Attributes of the geo object
     *
     * @return the attributes in a concatinated string
     */
    public String getAttributes() {
        return mAttributes;
    }

    @Override
    public Rectangle getBounds() {
        Rectangle bounds = mGeoObjectParts[0].getBounds();
        for (int i = 1; i < mGeoObjectParts.length; i++) {
            bounds.add(mGeoObjectParts[i].getBounds());
        }
        return bounds.getBounds();
    }

    @Override
    public String toString() {
        return "GeoObject{" +
                "mId='" + mId + '\'' +
                ", mType=" + mType +
                ", mGeoObjectParts=" + Arrays.toString(mGeoObjectParts) +
                ", mAttributes='" + mAttributes + '\'' +
                '}';
    }

    @Override
    public void paint(Graphics _g, Matrix _m) {
        PresentationSchema schema = DrawingContext.getInstance().getSchema(mType);

        Graphics2D _g2d = (Graphics2D) _g;

        if (mFillPloygon != null) {
            for (Polygon polygon : mFillPloygon) {
                if (polygon != null) {
                    _g2d.setColor(schema.getFillColor());
                    _g2d.fillPolygon(_m.multiply(polygon));
                }
            }
        }

        for (GeoObjectPart geoObjectPart : mGeoObjectParts) {
            geoObjectPart.draw(_g2d, _m, schema);
        }
    }
}
