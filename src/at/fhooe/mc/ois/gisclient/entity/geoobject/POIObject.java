package at.fhooe.mc.ois.gisclient.entity.geoobject;

import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class POIObject extends GeoObject {
    private BufferedImage mIcon;

    private Point mWorldCoordinates;

    public POIObject(URL _iconPath, Point _worldCoordinates) {
        super("000", 000, "");
        mWorldCoordinates = _worldCoordinates;
        try {
            mIcon = ImageIO.read(_iconPath);
        } catch (IOException _e) {
            _e.printStackTrace();
        }
    }

    @Override
    public void paint(Graphics _g, Matrix _m) {
        Point windowPoint = _m.multiply(mWorldCoordinates);
        _g.drawImage(mIcon, windowPoint.x, windowPoint.y, null);
    }
}
