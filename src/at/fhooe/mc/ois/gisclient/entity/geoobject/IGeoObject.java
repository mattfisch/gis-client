package at.fhooe.mc.ois.gisclient.entity.geoobject;

import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;

import java.awt.*;

/**
 * This interface represents a drawable geoobject
 */
public interface IGeoObject {

    /**
     * Liefert die Id des Geo-Objektes
     *
     * @return Die Id des Objektes
     * @see java.lang.String
     */
    public String getId();

    /**
     * Liefert den Typ des Geo-Objektes
     *
     * @return Der Typ des Objektes
     */
    public int getType();

    /**
     * Liefert die Geometrie des Geo-Objektes
     *
     * @return das Polygon des Objektes
     */
    public GeoObjectPart[] getGeoObjectParts();

    /**
     * Liefert die Bounding Box der Geometrie
     *
     * @return die Boundin Box der Geometrie als Rechteckobjekt
     * @see java.awt.Rectangle
     */
    public Rectangle getBounds();

    /**
     * Gibt die internen Informationen des Geo-Objektes als
     * String zurueck
     *
     * @return Der Inhalt des Objektes in Form eines Strings
     */
    public String toString();

    /**
     * Das Geo-Objekt verwendet den uebergebenen Graphik-Kontext,
     * um sich zu zeichnen
     *
     * @param _g Der Graphik-Kontext in dem sich das Objekt zeichnen soll
     * @param _m Die Transformationsmatrix, die für die Zeichenoperation
     *           verwendet werden soll
     * @see java.awt.Graphics
     * @see Matrix
     */
    public void paint(Graphics _g, Matrix _m);
}
