package at.fhooe.mc.ois.gisclient.entity.geoobject;

import at.fhooe.mc.ois.gisclient.util.PresentationSchema;
import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;

import java.awt.*;

/**
 * A class representing a point
 */
public class GeoPoint implements GeoObjectPart {

    /**
     * The geometry of the point
     */
    private final Point mGeometry;

    /**
     * Constructor
     *
     * @param _geometry the geometry of the point
     */
    public GeoPoint(Point _geometry) {
        mGeometry = _geometry;
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(mGeometry.x - 1, mGeometry.y - 1, 2, 2);
    }

    @Override
    public void draw(Graphics2D _g, Matrix _m, PresentationSchema _schema) {
        _g.setColor(_schema.getLineColor());
        _g.setStroke(new BasicStroke(_schema.getLineWidth()));
        Point pt = _m.multiply(mGeometry);
        _g.drawOval(pt.x, pt.y, 1, 1);
    }

    @Override
    public boolean contains(Point _point) {
        return getBounds().contains(_point);
    }
}
