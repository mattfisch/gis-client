package at.fhooe.mc.ois.gisclient.entity.geoobject;

import at.fhooe.mc.ois.gisclient.util.PresentationSchema;
import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;

import java.awt.*;

/**
 * This class represents a line
 */
public class GeoLine implements GeoObjectPart {

    /**
     * The lines geometry
     */
    private final Point[] mGeometry;

    /**
     * Constructor
     *
     * @param _geometry the lines geometry
     */
    public GeoLine(Point[] _geometry) {
        mGeometry = _geometry;
    }

    @Override
    public Rectangle getBounds() {
        Polygon container = new Polygon();
        for (Point point : mGeometry) {
            container.addPoint(point.x, point.y);
        }
        return container.getBounds();
    }

    @Override
    public void draw(Graphics2D _g, Matrix _m, PresentationSchema _schema) {
        _g.setColor(_schema.getLineColor());
        _g.setStroke(new BasicStroke(_schema.getLineWidth()));

        int[] xPts = new int[mGeometry.length];
        int[] yPts = new int[mGeometry.length];
        for (int i = 0; i < mGeometry.length; i++) {
            Point pt = _m.multiply(mGeometry[i]);
            xPts[i] = pt.x;
            yPts[i] = pt.y;
        }

        _g.drawPolyline(xPts, yPts, mGeometry.length);
    }

    @Override
    public boolean contains(Point _point) {
        Polygon container = new Polygon();
        for (Point point : mGeometry) {
            container.addPoint(point.x, point.y);
        }
        return container.contains(_point);
    }
}
