package at.fhooe.mc.ois.gisclient.entity.geodoublepoint;

public interface IGeoDoublePoint {
    /**
     * Berechnet die Länge des Vektors vom Ursprung
     * zum Punkt.
     *
     * @return Die Länge des Vektors
     */
    public double length();
}
