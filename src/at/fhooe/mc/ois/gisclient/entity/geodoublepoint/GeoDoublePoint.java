package at.fhooe.mc.ois.gisclient.entity.geodoublepoint;

/**
 * This class is used to calculate the scale factor with double precision
 */
public class GeoDoublePoint implements IGeoDoublePoint {

    /**
     * The coordinates of the point
     */
    private double mXCoordinate;
    private double mYCoordinate;

    /**
     * Der Konstruktor.
     *
     * @param _x x-Koordinate.
     * @param _y y-Koordinate.
     */
    public GeoDoublePoint(double _x, double _y) {
        mXCoordinate = _x;
        mYCoordinate = _y;
    }

    /**
     * Get x coordinate
     *
     * @return x coordinate with double precision
     */
    public double getX() {
        return mXCoordinate;
    }

    /**
     * Get y coordinate
     *
     * @return y coordinate with double precision
     */
    public double getY() {
        return mYCoordinate;
    }

    @Override
    public double length() {
        return Math.sqrt((Math.pow(mXCoordinate, 2) + Math.pow(mYCoordinate, 2)));
    }

    @Override
    public String toString() {
        return "GeoDoublePoint{" +
                "mXCoordinate=" + mXCoordinate +
                ", mYCoordinate=" + mYCoordinate +
                '}';
    }
}
