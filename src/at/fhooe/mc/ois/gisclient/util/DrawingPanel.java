package at.fhooe.mc.ois.gisclient.util;

import at.fhooe.mc.ois.gisclient.entity.geoobject.GeoObject;
import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;

/**
 * The drawing container extends the BufferedImage class to provide drawing functionality.
 * The Shape given to the drawing container will be drawen, when the BufferedImage is drawn.
 */
public class DrawingPanel extends BufferedImage {

    /**
     * The width of the image buffer
     */
    private int mWidth;

    /**
     * The height of the image buffer
     */
    private int mHeight;

    /**
     * Initializes a new instance of the Drawing Container.
     *
     * @param _width  the width, which the BufferedImage should have.
     * @param _height the height, which the BufferendImage should have.
     */
    public DrawingPanel(int _width, int _height) {
        super(_width, _height, BufferedImage.TYPE_INT_RGB);
        mWidth = _width;
        mHeight = _height;
    }

    /**
     * Set the content, which will be drawn inside of the BufferedImage
     */
    public void drawToContainer(Vector<GeoObject> _geoObjects, Matrix _matrix) {
        //clear old content
        flush();

        Graphics g = getGraphics();

        //draw the background
        g.setColor(Color.white);
        g.fillRect(0, 0, mWidth, mHeight);

        //draw the map
        g.setColor(Color.BLACK);
        _geoObjects.forEach(_geoObject -> _geoObject.paint(g, _matrix));
    }

    /**
     * Draws the DrawingPanel, using the graphics context
     *
     * @param _g the current graphics context
     * @see Graphics
     */
    public void draw(Graphics _g) {
        _g.drawImage(this, 0, 0, null);
    }
}
