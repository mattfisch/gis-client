package at.fhooe.mc.ois.gisclient.util.server;

import at.fhooe.mc.ois.gisclient.entity.geoobject.*;
import org.postgis.*;
import org.postgis.Point;
import org.postgis.Polygon;
import org.postgresql.PGConnection;
import org.postgresql.util.PGobject;

import java.awt.*;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * The PostgreServer containing Geo data information
 */
public class PostgreServer extends Server {

    /**
     * The active database connection
     */
    private Connection mServerConnection;

    /**
     * The query which will be executed on the next getData() call
     */
    private String mCurrentQuery;

    /**
     * the type of coordinates, which should be used
     */
    private String mCoordType;

    /**
     * Constructor
     *
     * @param _user      the user
     * @param _password  the users password
     * @param _ip        the servers ip address
     * @param _port      the servers port
     * @param _coordType the type of the coordinates, which to be loaded
     */
    protected PostgreServer(String _user, String _password, String _ip, String _port, String _coordType) {
        super(_user, _password, _ip, _port);
        mCoordType = _coordType;
    }

    @Override
    public boolean openConnection() {
        try {
            //Load the JDBC driver and establish a connection.
            Class.forName("org.postgresql.Driver");
            String url =
                    "jdbc:postgresql://" + mIp + ":" + mPort + "/" + mCoordType;

            System.out.println("URL:" + url);

            mServerConnection = DriverManager.getConnection(url, "geo", "geo");
            //Add the geometry types to the connection.
            PGConnection c = (PGConnection) mServerConnection;

            c.addDataType("geometry", (Class<? extends PGobject>) Class.forName("org.postgis.PGgeometry"));
            c.addDataType("box2d", (Class<? extends PGobject>) Class.forName("org.postgis.PGbox2d"));
        } catch (Exception _e) {
            _e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean closeConnection() {
        try {
            mServerConnection.close();
            mServerConnection = null;
            return true;
        } catch (SQLException _e) {
            _e.printStackTrace();
            return false;
        }
    }

    @Override
    public void setQuery(String _query) {
        mCurrentQuery = _query;
    }

    @Override
    public void getData() {
        //Create a statement and execute a select query.
        try {
            Statement s = mServerConnection.createStatement();
            ResultSet r = s.executeQuery(mCurrentQuery);

            while (r.next()) {
                String id = r.getString("id");
                int type = r.getInt("type");
                String attributes = r.getString("attr");
                PGgeometry geom = (PGgeometry) r.getObject("geom");

                java.awt.Polygon[] fillPolys = null;
                LinkedList<GeoObjectPart> contentGeoObjectsParts = new LinkedList<>();
                switch (geom.getGeoType()) {
                    case Geometry.MULTIPOLYGON: {
                        String wkt = geom.toString();
                        MultiPolygon multiPolygon = new MultiPolygon(wkt);
                        fillPolys = new java.awt.Polygon[multiPolygon.numPolygons()];
                        for (int i = 0; i < multiPolygon.getPolygons().length; i++) {
                            fillPolys[i] = getFillPoly(multiPolygon.getPolygon(i));
                            contentGeoObjectsParts.addAll(getGeoObjectParts(multiPolygon.getPolygon(i)));
                        }
                    }
                    break;
                    case (Geometry.POLYGON): {
                        String wkt = geom.toString();
                        org.postgis.Polygon p = new org.postgis.Polygon(wkt);
                        fillPolys = new java.awt.Polygon[1];
                        fillPolys[0] = getFillPoly(p);
                        contentGeoObjectsParts.addAll(getGeoObjectParts(p));
                    }
                    break;
                    case (Geometry.LINESTRING): {
                        String wkt = geom.toString();
                        org.postgis.LineString line = new LineString(wkt);

                        org.postgis.Point[] points = line.getPoints();
                        java.awt.Point[] awtPoints = new java.awt.Point[points.length];

                        for (int i = 0; i < points.length; i++) {
                            awtPoints[i] = new java.awt.Point((int) points[i].getX(), (int) points[i].getY());
                        }
                        contentGeoObjectsParts.add(new GeoLine(awtPoints));
                    }
                    break;
                    case (Geometry.MULTILINESTRING): {
                        String wkt = geom.toString();
                        org.postgis.MultiLineString lines = new MultiLineString(wkt);
                        for (LineString lineString : lines.getLines()) {
                            org.postgis.Point[] points = lineString.getPoints();
                            java.awt.Point[] awtPoints = new java.awt.Point[points.length];

                            for (int i = 0; i < points.length; i++) {
                                awtPoints[i] = new java.awt.Point((int) points[i].getX(), (int) points[i].getY());
                            }
                            contentGeoObjectsParts.add(new GeoLine(awtPoints));
                        }
                    }
                    break;
                    case (Geometry.POINT): {
                        String wkt = geom.toString();
                        org.postgis.Point pt = new org.postgis.Point(wkt);
                        contentGeoObjectsParts.add(new GeoPoint(new java.awt.Point((int) pt.getX(), (int) pt.getY())));
                    }
                }

                if (contentGeoObjectsParts.isEmpty()) {
                    super.notifyError("Content Polygon was null");
                    System.out.println("GeomID was: " + geom.getGeoType());
                } else {
                    GeoObjectPart[] contentPolysArr = contentGeoObjectsParts.toArray(new GeoObjectPart[contentGeoObjectsParts.size()]);
                    GeoObject geoObject = new GeoObject(id, type, contentPolysArr, attributes, fillPolys);
                    super.notifyData(geoObject);
                }
            }
        } catch (Exception _e) {
            _e.printStackTrace();
            super.notifyError(_e.getLocalizedMessage());
        }
        super.notifyDone();
    }

    /**
     * Gets the fill polygon from the postgis polygon
     *
     * @param _from the postgis polygon, from which to build the java awt polygon
     * @return a java awt polygon
     */
    private java.awt.Polygon getFillPoly(org.postgis.Polygon _from) {
        java.awt.Polygon into;
        if (_from.numRings() == 1) {
            into = new java.awt.Polygon();
            for (int j = 0; j < _from.getRing(0).numPoints(); j++) {
                org.postgis.Point pPG = _from.getRing(0).getPoint(j);
                into.addPoint((int) pPG.x, (int) pPG.y);
            }
        } else {
            System.out.println("Multi polygon detected!");
            MultiPolygonHelper helper = new MultiPolygonHelper(_from);
            into = helper.getFillPolygon();
        }
        return into;
    }

    /**
     * Retrieves the polygon, which makes up the frame
     *
     * @param _from the postgis polygon, from which to get the data
     * @return the java awt polygon, from the postgis data
     */
    private List<GeoObjectPart> getGeoObjectParts(org.postgis.Polygon _from) {
        LinkedList<GeoObjectPart> parts = new LinkedList<>();
        for (int i = 0; i < _from.numRings(); i++) {
            java.awt.Polygon polygon = new java.awt.Polygon();
            for (Point point : _from.getRing(i).getPoints()) {
                polygon.addPoint((int) point.getX(), (int) point.getY());
            }
            parts.add(new GeoArea(polygon));
        }
        return parts;
    }

    /**
     * Private helper class, transforming a multipolygon to a single awt.Polygon
     */
    private class MultiPolygonHelper {

        /**
         * The polygon, for which to connect the holes
         */
        private org.postgis.Polygon mPolygon;

        /**
         * The anchor points, connection the rings
         */
        private int[] mAnchorPointsIndex;

        /**
         * A member, indicating the current, smallest distance between two polygons
         * in an multipolygon
         */
        private double mShortestDistanceBetweenRings;

        /**
         * Helper member for accessing the right index in the mAnchorPoints array
         */
        private int mCurrentPointIndex;

        /**
         * Create a new instance of the helper, calculating the anchor points between rings
         *
         * @param _polygon the polygon, for which rings to create the fill polygon
         */
        private MultiPolygonHelper(org.postgis.Polygon _polygon) {
            mPolygon = _polygon;
            mAnchorPointsIndex = new int[mPolygon.numRings() * 2 - 2];
            calculateAnchorPointsIndices();
        }

        /**
         * Calculates the anchor points between the rings
         */
        private void calculateAnchorPointsIndices() {
            mCurrentPointIndex = 0;
            for (int i = 0; i < mPolygon.numRings() - 1; i++) {
                mShortestDistanceBetweenRings = Integer.MAX_VALUE;

                LinearRing currentPolyRing = mPolygon.getRing(i);
                LinearRing nextPolyRing = mPolygon.getRing(i + 1);

                for (int i1 = 0; i1 < currentPolyRing.getPoints().length; i1++) {
                    for (int i2 = 0; i2 < nextPolyRing.getPoints().length; i2++) {
                        double currentDist = currentPolyRing.getPoint(i1).distance(nextPolyRing.getPoint(i2));
                        if (currentDist < mShortestDistanceBetweenRings) {
                            mShortestDistanceBetweenRings = currentDist;
                            mAnchorPointsIndex[mCurrentPointIndex] = i1;
                            mAnchorPointsIndex[mCurrentPointIndex + 1] = i2;
                        }
                    }
                }
            }
            mCurrentPointIndex += 2;
        }

        /**
         * Retrieve the fill polygon
         *
         * @return the fill polygon
         */
        public java.awt.Polygon getFillPolygon() {
            java.awt.Polygon polygon = new java.awt.Polygon();
            drawPolygon(mPolygon.getRing(0), polygon, 0, 0);
            return polygon;
        }

        /**
         * recursively draws the polygons, keeping a small connection line between them
         */
        private void drawPolygon(LinearRing _from, java.awt.Polygon _into, int _startPt, int _currRing) {
            //start drawing the polygon, until we need to draw in inner circle
            for (int i = _startPt; i < _from.getPoints().length; i++) {
                //when our current polygon (zero-indexed) times two is out of bounds of the
                // anchor points array, we reached the last polygon of the multipolygon
                if ((_currRing * 2) < mAnchorPointsIndex.length)
                    if (i == mAnchorPointsIndex[(_currRing * 2)]) {
                        LinearRing ring = mPolygon.getRing(_currRing + 1);
                        drawPolygon(ring, _into, mAnchorPointsIndex[_currRing * 2 + 1], _currRing + 1);
                    }
                org.postgis.Point pt = _from.getPoint(i);
                _into.addPoint((int) pt.getX(), (int) pt.getY());
            }

            //draws the rest of the polygon
            for (int i = 0; i <= _startPt; i++) {
                //when our current polygon (zero-indexed) times two is out of bounds of the
                // anchor points array, we reached the last polygon of the multipolygon
                if ((_currRing * 2) < mAnchorPointsIndex.length)
                    if (i == mAnchorPointsIndex[(_currRing * 2)]) {
                        LinearRing ring = mPolygon.getRing(_currRing + 1);
                        drawPolygon(ring, _into, mAnchorPointsIndex[_currRing * 2 + 1], _currRing + 1);
                    }
                org.postgis.Point pt = _from.getPoint(i);
                _into.addPoint((int) pt.getX(), (int) pt.getY());
            }

            //if we are inside of an inner ring, we add the connection back to the last ring, or the outer ring
            if (_currRing > 0) {
                org.postgis.Point pt = mPolygon.getRing(_currRing - 1).getPoint(mAnchorPointsIndex[(_currRing - 1)] - 1);
                _into.addPoint((int) pt.getX(), (int) pt.getY());
            }
        }
    }
}