package at.fhooe.mc.ois.gisclient.util.server;

import at.fhooe.mc.ois.gisclient.entity.geoobject.GeoObject;
import at.fhooe.mc.ois.gisclient.util.DrawingPanel;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/**
 * Baseclass of all servers used by the application
 */
public abstract class Server {

    /**
     * Calls all observers, when a new geodata object is available, the current query is done or an error occured
     */
    public interface ServerObserver {
        void onNewDataAvailable(GeoObject _object);

        void onFetchingDataDone();

        void onError(String _error);
    }

    /**
     * user, used to connect to dummy server
     */
    protected String mUser;

    /**
     * password, used to connect to dummy server
     */
    protected String mPassword;

    /**
     * ip, used to connect to dummy server
     */
    protected String mIp;

    /**
     * port, used to connect to dummy server
     */
    protected String mPort;

    /**
     * A list of subscribed observers
     */
    protected List<ServerObserver> mServerObserverList;

    /**
     * Init a new DummyServer Instance
     *
     * @param _user     user to connect
     * @param _password password of the user
     * @param _ip       ip of the server
     * @param _port     port of the server
     */
    protected Server(String _user, String _password, String _ip, String _port) {
        mServerObserverList = new LinkedList<>();
        mUser = _user;
        mPassword = _password;
        mIp = _ip;
        mPort = _port;
    }

    /**
     * Add a new Observer to the server
     *
     * @param _observer the observer
     */
    public void addObserver(ServerObserver _observer) {
        mServerObserverList.add(_observer);
    }

    /**
     * Remove an Observer from the list
     *
     * @param _observer the observer, which will be removed
     */
    public void removeObserver(ServerObserver _observer) {
        mServerObserverList.remove(_observer);
    }

    /**
     * Clears all references to observers
     */
    public void disposeObservers() {
        mServerObserverList.clear();
    }

    /**
     * Notify observers about new data
     *
     * @param _data the new data available
     */
    protected void notifyData(GeoObject _data) {
        mServerObserverList.forEach(_obs -> _obs.onNewDataAvailable(_data));
    }

    /**
     * Notify oberservs, about finished job
     */
    protected void notifyDone() {
        mServerObserverList.forEach(ServerObserver::onFetchingDataDone);
    }

    /**
     * Notify observers about error
     */
    protected void notifyError(String _error) {
        mServerObserverList.forEach(_observer -> _observer.onError(_error));
    }

    /**
     * Opens the connection to the server
     *
     * @return true, when successful, false otherwise
     */
    public abstract boolean openConnection();

    /**
     * Closes the connection to the server
     *
     * @return true, if closing was successful
     */
    public abstract boolean closeConnection();

    /**
     * Sets the query, which will be executed next by the current server instance
     *
     * @param _query the query, which will be executed next
     */
    public abstract void setQuery(String _query);

    /**
     * Calls the server, to start retrieving the data
     */
    public abstract void getData();
}
