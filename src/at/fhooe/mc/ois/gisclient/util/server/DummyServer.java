package at.fhooe.mc.ois.gisclient.util.server;

import at.fhooe.mc.ois.gisclient.entity.geoobject.GeoArea;
import at.fhooe.mc.ois.gisclient.entity.geoobject.GeoObject;
import de.intergis.JavaClient.comm.*;
import de.intergis.JavaClient.gui.IgcConnection;

import java.awt.*;
import java.io.IOException;

/**
 * Created by Vortech on 21/04/2017.
 */
public class DummyServer extends Server {

    /**
     * Die Verbindung zum Geo-Server
     */
    CgGeoConnection m_geoConnection = null;

    /**
     * das Anfrage-Interface des Geo-Servers
     */
    CgGeoInterface m_geoInterface = null;

    /**
     * The query which will be executed on the next getData() call
     */
    private String mCurrentQuery;

    /**
     * Constructor
     *
     * @param _user     the user
     * @param _password the users password
     * @param _ip       the servers ip address
     * @param _port     the servers port
     */
    protected DummyServer(String _user, String _password, String _ip, String _port) {
        super(_user, _password, _ip, _port);
    }

    @Override
    public boolean openConnection() {
        try {
            // der Geo-Server wird initialisiert
            m_geoConnection =
                    new IgcConnection(new CgConnection(mUser, mPassword,
                            "T:" + mIp + ":" + mPort, null));
            // das Anfrage-Interface des Servers wird abgeholt
            m_geoInterface = m_geoConnection.getInterface();
            return true;
        } catch (Exception _e) {
            _e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean closeConnection() {
        try {
            m_geoConnection.Quit();
            m_geoConnection.dispose();
            m_geoConnection = null;
            m_geoInterface = null;
        } catch (Exception _e) {
            _e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void setQuery(String _query) {
        mCurrentQuery = _query;
    }

    @Override
    public void getData() {
        if (mCurrentQuery == null) {
            System.out.println("NO QUERY SPECIFIED! ");
            return;
        }

        Thread thread = new Thread(() -> {
            try {
                CgStatement stmt = m_geoInterface.Execute(mCurrentQuery);
                CgResultSet cursor = stmt.getCursor();
                while (cursor.next()) {
                    CgIGeoObject obj = cursor.getObject();
                    System.out.println("NAME --> " + obj.getName());
                    System.out.println("TYP  --> " + obj.getCategory());
                    CgIGeoPart[] parts = obj.getParts();
                    for (int i = 0; i < parts.length; i++) {
                        System.out.println("PART " + i);
                        int pointCount = parts[i].getPointCount();
                        int[] xArray = parts[i].getX();
                        int[] yArray = parts[i].getY();
                        Polygon poly = new Polygon(xArray, yArray, pointCount);
                        for (int j = 0; j < pointCount; j++) {
                            System.out.println("[" + xArray[j] + " ; " + yArray[j] + "]");
                        } // for j
                        GeoObject object = new GeoObject(obj.getName(), obj.getCategory(), "");
                        object.setGeoObjectPart(new GeoArea(poly));
                        super.notifyData(object);
                    } // for i
                } // while cursor
            } catch (Exception _e) {
                _e.printStackTrace();
                super.notifyError(_e.getLocalizedMessage());
            }

            super.notifyDone();
        });
        thread.start();
    }
}
