package at.fhooe.mc.ois.gisclient.util.server;

/**
 * Represents the currently used server
 */
public class ServerManager {

    /**
     * The types, the currently used server instance can have
     */
    public enum SERVER_TYPE {
        DUMMY,
        POSTGRE
    }

    /**
     * The static instance of the server, used by the singleton pattern
     */
    private static Server mServer;

    /**
     * Returns the currently used server or initializes a new one and closes the old one if servertype is switched
     *
     * @param _type the type of the requested server
     * @return an instance of the requested server
     */
    public static Server getInstance(SERVER_TYPE _type) {
        switch (_type) {
            case DUMMY: {
                if (mServer != null && mServer instanceof DummyServer) {
                    return mServer;
                } else if (mServer instanceof PostgreServer) {
                    mServer.closeConnection();
                    mServer.disposeObservers();
                }
                mServer = new DummyServer("admin", "admin", "10.29.0.146", "4949");
                return mServer;
            }
            case POSTGRE: {
                if (mServer != null && mServer instanceof PostgreServer) {
                    return mServer;
                } else if (mServer instanceof DummyServer) {
                    mServer.closeConnection();
                    mServer.disposeObservers();
                }
                mServer = new PostgreServer("geo", "geo", "localhost", "5432", "osm");
                return mServer;
            }
        }
        return null;
    }
}
