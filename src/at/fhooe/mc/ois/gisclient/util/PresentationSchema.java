package at.fhooe.mc.ois.gisclient.util;

import java.awt.*;
import java.util.Hashtable;

/**
 * Tells the system, how to paint a geo object
 */
public class PresentationSchema {
    /**
     * The objects line color
     */
    private Color mLineColor = null;

    /**
     * The objects fill color
     */
    private Color mFillColor = null;

    /**
     * The objects line width
     */
    private float mLineWidth = 1.0f;

    /**
     * Constructor
     *
     * @param _fillColor the fill color of the schema
     * @param _lineColor the line color of the schema
     */
    public PresentationSchema(Color _fillColor, Color _lineColor) {
        mLineColor = _lineColor;
        mFillColor = _fillColor;
    }

    /**
     * Returns the line color
     *
     * @return line color
     */
    public Color getLineColor() {
        return mLineColor;
    }

    /**
     * Returns the fill color
     *
     * @return fill color
     */
    public Color getFillColor() {
        return mFillColor;
    }

    /**
     * Returns the line width
     *
     * @return line width
     */
    public float getLineWidth() {
        return mLineWidth;
    }

}
