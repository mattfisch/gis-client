package at.fhooe.mc.ois.gisclient.util;

import java.awt.*;
import java.util.Hashtable;

/**
 * Mapps the geo objects type to a PresentationSchema
 */
public class DrawingContext {

    /**
     * Drawing context singleton instance
     */
    private static DrawingContext mInstance;

    /**
     * keeps track of all the available drawing schemas
     */
    private Hashtable<Integer, PresentationSchema> mSchemata;

    /**
     * Initializes all the available drawing schemas
     */
    private DrawingContext() {
        mSchemata = new Hashtable<>();

        mSchemata.put(233, new PresentationSchema(Color.WHITE, Color.BLACK));
        mSchemata.put(931, new PresentationSchema(Color.RED, Color.BLACK));
        mSchemata.put(932, new PresentationSchema(Color.ORANGE, Color.RED));
        mSchemata.put(933, new PresentationSchema(Color.WHITE, Color.WHITE));
        mSchemata.put(934, new PresentationSchema(Color.WHITE, Color.WHITE));

        mSchemata.put(8002, new PresentationSchema(Color.WHITE, Color.BLACK));

        mSchemata.put(9001, new PresentationSchema(Color.WHITE, Color.BLACK));
        mSchemata.put(9007, new PresentationSchema(Color.BLACK, Color.YELLOW));
        mSchemata.put(9026, new PresentationSchema(Color.DARK_GRAY, Color.YELLOW));

        mSchemata.put(5004, new PresentationSchema(Color.GREEN, Color.GREEN));
        mSchemata.put(5006, new PresentationSchema(Color.GREEN, Color.BLACK));

        //grasland
        mSchemata.put(6001, new PresentationSchema(new Color(82, 191, 55), new Color(82, 191, 55)));
        //wood
        mSchemata.put(6002, new PresentationSchema(new Color(55, 127, 37), new Color(55, 127, 37)));
        //scrub
        mSchemata.put(6003, new PresentationSchema(new Color(110, 255, 73), new Color(110, 255, 73)));
        //fell
        mSchemata.put(6004, new PresentationSchema(new Color(112, 191, 55), new Color(112, 191, 55)));
        //water
        mSchemata.put(6005, new PresentationSchema(new Color(34, 62, 191), new Color(34, 62, 191)));
        //land
        mSchemata.put(6006, new PresentationSchema(new Color(229, 210, 120), new Color(229, 210, 120)));
        //rock
        mSchemata.put(6007, new PresentationSchema(new Color(127, 125, 118), new Color(127, 125, 118)));
        //beach
        mSchemata.put(6008, new PresentationSchema(new Color(255, 237, 136), new Color(255, 237, 136)));
        //coastline
        mSchemata.put(6009, new PresentationSchema(new Color(229, 213, 122), new Color(229, 213, 122)));
        //barerock
        mSchemata.put(6010, new PresentationSchema(new Color(64, 63, 57), new Color(64, 63, 57)));
        //scree
        mSchemata.put(6011, new PresentationSchema(new Color(127, 126, 115), new Color(127, 126, 115)));
        //wetland
        mSchemata.put(6012, new PresentationSchema(new Color(199, 255, 223), new Color(199, 255, 223)));
        //cliff
        mSchemata.put(6013, new PresentationSchema(new Color(127, 84, 13), new Color(127, 84, 13)));
        //tree
        mSchemata.put(6014, new PresentationSchema(new Color(26, 64, 8), new Color(26, 64, 8)));
        //peak
        mSchemata.put(6015, new PresentationSchema(new Color(250, 255, 242), new Color(250, 255, 242)));

        mSchemata.put(1101, new PresentationSchema(Color.MAGENTA, Color.GREEN));
        mSchemata.put(1030, new PresentationSchema(Color.ORANGE, Color.ORANGE));
        mSchemata.put(1031, new PresentationSchema(Color.ORANGE, Color.ORANGE));
        mSchemata.put(1040, new PresentationSchema(Color.ORANGE, Color.ORANGE));
        mSchemata.put(1041, new PresentationSchema(Color.ORANGE, Color.ORANGE));
        mSchemata.put(1050, new PresentationSchema(Color.ORANGE, Color.ORANGE));
        mSchemata.put(1051, new PresentationSchema(Color.ORANGE, Color.ORANGE));
        mSchemata.put(1090, new PresentationSchema(Color.ORANGE, Color.ORANGE));
        mSchemata.put(1091, new PresentationSchema(Color.ORANGE, Color.ORANGE));
    }

    /**
     * Get the schema of the type
     *
     * @param _type the geo objects type
     * @return the geo objects corresponding PresentationSchema
     */
    public PresentationSchema getSchema(int _type) {
        PresentationSchema schema = mSchemata.get(_type);
        if (schema == null)
            return new PresentationSchema(Color.BLACK, Color.YELLOW);
        else
            return schema;
    }

    /**
     * Get an instance of the singleton DrawingContext
     *
     * @return an instance of DrawingContext
     */
    public static DrawingContext getInstance() {
        if (mInstance == null) {
            return new DrawingContext();
        } else {
            return mInstance;
        }
    }
}
