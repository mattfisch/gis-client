package at.fhooe.mc.ois.gisclient.util.matrix;

import at.fhooe.mc.ois.gisclient.entity.geodoublepoint.GeoDoublePoint;

import java.awt.*;

/**
 * Created by Vortech on 06/04/2017.
 */
public class Matrix implements IMatrix {

    /**
     * Our matrix, which will be used for internal calculations
     */
    private double m11, m12, m13;
    private double m21, m22, m23;
    private double m31, m32, m33;

    /**
     * Standardkonstruktor
     */
    public Matrix() {
        //values are allocated with zero by default.
    }

    /**
     * Standardkonstruktor
     *
     * @param _m11 Der Wert des Matrix Feldes 1x1 (Zeile1/Spalte1)
     * @param _m12 Der Wert des Matrix Feldes 1x2 (Zeile1/Spalte2)
     *             …
     */
    public Matrix(double _m11, double _m12, double _m13,
                  double _m21, double _m22, double _m23,
                  double _m31, double _m32, double _m33) {
        m11 = _m11;
        m12 = _m12;
        m13 = _m13;
        m21 = _m21;
        m22 = _m22;
        m23 = _m23;
        m31 = _m31;
        m32 = _m32;
        m33 = _m33;
    }

    /**
     * Liefert eine Translationsmatrix
     *
     * @param _x Der Translationswert der Matrix in X-Richtung
     * @param _y Der Translationswert der Matrix in Y-Richtung
     * @return Die Translationsmatrix
     */
    public static Matrix translate(double _x, double _y) {
        return new Matrix(1, 0, _x, 0, 1, _y, 0, 0, 1);
    }

    /**
     * Liefert eine Translationsmatrix
     *
     * @param _pt Ein Punkt, der die Translationswerte enthält
     * @return Die Translationsmatrix
     * @see java.awt.Point
     */
    public static Matrix translate(java.awt.Point _pt) {
        return translate(_pt.x, _pt.y);
    }

    /**
     * Liefert eine Skalierungsmatrix
     *
     * @param _scaleVal Der Skalierungswert der Matrix
     * @return Die Skalierungsmatrix
     */
    public static Matrix scale(double _scaleVal) {
        return new Matrix(_scaleVal, 0, 0, 0, _scaleVal, 0, 0, 0, 1);
    }

    /**
     * Liefert eine Spiegelungsmatrix (X-Achse)
     *
     * @return Die Spiegelungsmatrix
     */
    public static Matrix mirrorX() {
        return new Matrix(1, 0, 0, 0, -1, 0, 0, 0, 1);
    }

    /**
     * Liefert eine Spiegelungsmatrix (Y-Achse)
     *
     * @return Die Spiegelungsmatrix
     */
    public static Matrix mirrorY() {
        return new Matrix(-1, 0, 0, 0, 1, 0, 0, 0, 1);
    }

    /**
     * Liefert eine Rotationsmatrix
     *
     * @param _alpha Der Winkel (in rad), um den rotiert werden
     *               soll
     * @return Die Rotationsmatrix
     */
    public static Matrix rotate(double _alpha) {
        return new Matrix(Math.cos(_alpha), -Math.sin(_alpha), 0, Math.sin(_alpha), Math.cos(_alpha), 0, 0, 0, 1);
    }

    /**
     * Liefert den Faktor, der benötigt wird, um das _world-
     * Rechteck in das _win-Rechteck zu skalieren (einzupassen)
     * bezogen auf die X-Achse  Breite
     *
     * @param _world Das Rechteck in Weltkoordinaten
     * @param _win   Das Rechteck in Bildschirmkoordinaten
     * @return Der Skalierungsfaktor
     * @see java.awt.Rectangle
     */
    public static double getZoomFactorX(java.awt.Rectangle _world,
                                        java.awt.Rectangle _win) {
        return _win.getWidth() / _world.getWidth();
    }

    /**
     * Liefert den Faktor, der benötigt wird, um das _world-
     * Rechteck in das _win-Rechteck zu skalieren (einzupassen)
     * bezogen auf die Y-Achse  Höhe
     *
     * @param _world Das Rechteck in Weltkoordinaten
     * @param _win   Das Rechteck in Bildschirmkoordinaten
     * @return Der Skalierungsfaktor
     * @see java.awt.Rectangle
     */
    public static double getZoomFactorY(java.awt.Rectangle _world,
                                        java.awt.Rectangle _win) {
        return _win.getHeight() / _world.getHeight();
    }

    /**
     * Liefert eine Matrix, die alle notwendigen Transformationen
     * beinhaltet (Translation, Skalierung, Spiegelung und
     * Translation), um ein _world-Rechteck in ein _win-Rechteck
     * abzubilden
     *
     * @param _world Das Rechteck in Weltkoordinaten
     * @param _win   Das Rechteck in Bildschirmkoordinaten
     * @return Die Transformationsmatrix
     * @see java.awt.Rectangle
     */
    public static Matrix zoomToFit(java.awt.Rectangle _world,
                                   java.awt.Rectangle _win) {
        Double zoomX = getZoomFactorX(_world, _win);
        Double zoomY = getZoomFactorY(_world, _win);
        Double usedZoom = zoomX <= zoomY ? zoomX : zoomY;

        Matrix translationCenter = translate(-_world.getCenterX(), -_world.getCenterY());
        Matrix scale = scale(usedZoom);
        Matrix mirror = mirrorX();
        Matrix translationBack = translate(_win.getCenterX(), _win.getCenterY());

        return translationBack.multiply(mirror).multiply(scale).multiply(translationCenter);
    }

    /**
     * Liefert eine Matrix, die eine vorhandene Transformations-
     * matrix erweitert, um an einem bestimmten Punkt um einen
     * bestimmten Faktor in die Karte hinein- bzw. heraus zu
     * zoomen
     *
     * @param _old       Die zu erweiternde Transformationsmatrix
     * @param _zoomPt    Der Punkt an dem gezoomt werden soll
     * @param _zoomScale Der Zoom-Faktor um den gezoomt werden
     *                   soll
     * @return Die neue Transformationsmatrix
     * @see java.awt.Point
     */
    public static Matrix zoomPoint(Matrix _old,
                                   java.awt.Point _zoomPt,
                                   double _zoomScale) {
        Matrix translationCenter = translate(-_zoomPt.x, -_zoomPt.y);
        Matrix scale = scale(_zoomScale);
        Matrix translationBack = translate(_zoomPt);
        return translationBack.multiply(scale).multiply(translationCenter).multiply(_old);
    }

    /**
     * Creates an identity matrix
     *
     * @return a 3 x 3 identity matrix
     */
    public static Matrix getIdentityMatrix() {
        return new Matrix(1, 0, 0, 0, 1, 0, 0, 0, 1);
    }

    @Override
    public Matrix invers() {
        //Calculate Adjunctions
        double x11 = m22 * m33 - m23 * m32;
        double x12 = m13 * m32 - m12 * m33;
        double x13 = m12 * m23 - m13 * m22;
        double x21 = m23 * m31 - m21 * m33;
        double x22 = m11 * m33 - m13 * m31;
        double x23 = m13 * m21 - m11 * m23;
        double x31 = m21 * m32 - m22 * m31;
        double x32 = m12 * m31 - m11 * m32;
        double x33 = m11 * m22 - m12 * m21;

        //calculate the Determinant
        double A = m11 * m22 * m33 +
                m12 * m23 * m31 +
                m13 * m21 * m32 -
                m11 * m23 * m32 -
                m12 * m21 * m33 -
                m13 * m22 * m31;

        double Aabs = 1 / Math.abs(A);

        return new Matrix(
                x11 * Aabs, x12 * Aabs, x13 * Aabs,
                x21 * Aabs, x22 * Aabs, x23 * Aabs,
                x31 * Aabs, x32 * Aabs, x33 * Aabs);
    }

    @Override
    public Matrix multiply(Matrix _other) {
        return new Matrix(
                m11 * _other.m11 + m12 * _other.m21 + m13 * _other.m31,
                m11 * _other.m12 + m12 * _other.m22 + m13 * _other.m32,
                m11 * _other.m13 + m12 * _other.m23 + m13 * _other.m33,

                m21 * _other.m11 + m22 * _other.m21 + m23 * _other.m31,
                m21 * _other.m12 + m22 * _other.m22 + m23 * _other.m32,
                m21 * _other.m13 + m22 * _other.m23 + m23 * _other.m33,

                m31 * _other.m11 + m32 * _other.m21 + m33 * _other.m31,
                m31 * _other.m12 + m32 * _other.m22 + m33 * _other.m32,
                m31 * _other.m13 + m32 * _other.m23 + m33 * _other.m33
        );
    }

    @Override
    public Point multiply(Point _pt) {
        return new Point(
                (int) (m11 * _pt.getX() + m12 * _pt.getY() + m13),
                (int) (m21 * _pt.getX() + m22 * _pt.getY() + m23));
    }

    @Override
    public GeoDoublePoint multiply(GeoDoublePoint _pt) {
        double srcx = _pt.getX();
        double srcy = _pt.getY();
        double destx = m11 * srcx + m12 * srcy;
        double desty = m21 * srcx + m22 * srcy;
        return new GeoDoublePoint(destx, desty);
    }

    @Override
    public Rectangle multiply(Rectangle _rect) {
        Rectangle rect = new Rectangle(multiply(new Point(_rect.x, _rect.y)));
        rect.add(multiply(new Point(_rect.x + _rect.width, _rect.y + _rect.height)));
        return rect;
    }

    @Override
    public Polygon multiply(Polygon _poly) {
        Polygon newPoly = new Polygon();
        for (int i = 0; i < _poly.npoints; i++) {
            Point newPoint = multiply(new Point(_poly.xpoints[i], _poly.ypoints[i]));
            newPoly.addPoint(newPoint.x, newPoint.y);
        }
        return newPoly;
    }

    public Polygon[] multiply(Polygon[] _polys) {
        Polygon[] polysnew = new Polygon[_polys.length];
        for (int i = 0; i < _polys.length; i++) {
            polysnew[i] = multiply(_polys[i]);
        }
        return polysnew;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(m11 + " - ").append(m12 + " - ").append(m13).append("\n");
        builder.append(m21 + " - ").append(m22 + " - ").append(m23).append("\n");
        builder.append(m31 + " - ").append(m32 + " - ").append(m33).append("\n");
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Matrix)) {
            throw new ClassCastException("Can't cast class to Matrix");
        }
        Matrix other = (Matrix) obj;
        return (isRelativelyEqual(m11, other.m11)) && (isRelativelyEqual(m12, other.m12)) && (isRelativelyEqual(m13, other.m13)) &&
                (isRelativelyEqual(m21, other.m21)) && (isRelativelyEqual(m22, other.m22)) && (isRelativelyEqual(m23, other.m23)) &&
                (isRelativelyEqual(m31, other.m31)) && (isRelativelyEqual(m32, other.m32)) && (isRelativelyEqual(m33, other.m33));
    }

    /**
     * Compares, if two double values are relatively equal
     *
     * @param _d1 double value one
     * @param _d2 double value two
     * @return true, if they are relatively equal with a fixed delta of 1
     */
    private boolean isRelativelyEqual(double _d1, double _d2) {
        return isRelativelyEqual(_d1, _d2, 1);
    }

    /**
     * Compares, if two double values are relatively equal
     *
     * @param _d1    double value one
     * @param _d2    double value two
     * @param _delta the delta value, to which they are compared
     * @return true, if they are relatively equal
     */
    private boolean isRelativelyEqual(double _d1, double _d2, double _delta) {
        return _delta > Math.abs(_d1 - _d2) / Math.max(Math.abs(_d1), Math.abs(_d2));
    }
}
