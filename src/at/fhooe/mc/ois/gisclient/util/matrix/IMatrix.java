package at.fhooe.mc.ois.gisclient.util.matrix;

import at.fhooe.mc.ois.gisclient.entity.geodoublepoint.GeoDoublePoint;

/**
 * This interface represents all manipulations, which can be done using an implementation of this interface
 */
public interface IMatrix {

    /**
     * Liefert eine String-Repräsentation der Matrix
     *
     * @return Ein String mit dem Inhalt der Matrix
     * @see java.lang.String
     */
    public String toString();

    /**
     * Liefert die Invers-Matrix der Transformationsmatrix
     *
     * @return Die Invers-Matrix
     */
    public Matrix invers();

    /**
     * Liefert eine Matrix, die das Ergebnis einer Matrizen-
     * multiplikation zwischen dieser und der übergebenen Matrix
     * ist
     *
     * @param _other Die Matrix mit der Multipliziert werden soll
     * @return Die Ergebnismatrix der Multiplikation
     */
    public Matrix multiply(Matrix _other);

    /**
     * Multipliziert einen Punkt mit der Matrix und liefert das
     * Ergebnis der Multiplikation zurück
     *
     * @param _pt Der Punkt, der mit der Matrix multipliziert
     *            werden soll
     * @return Ein neuer Punkt, der das Ergebnis der
     * Multiplikation repräsentiert
     * @see java.awt.Point
     */
    public java.awt.Point multiply(java.awt.Point _pt);

    /**
     * Multipliziert einen GeoDoublePoint mit der Matrix und liefert das
     * Ergebnis der Multiplikation zurück
     *
     * @param _pt Der GeoDoublePoint, der mit der Matrix multipliziert
     *            werden soll
     * @return Ein neuer GeoDoublePoint, der das Ergebnis der
     * Multiplikation repräsentiert
     * @see GeoDoublePoint
     */
    public GeoDoublePoint multiply(GeoDoublePoint _pt);

    /**
     * Multipliziert ein Rechteck mit der Matrix und liefert das
     * Ergebnis der Multiplikation zurück
     *
     * @param _rect Das Rechteck, das mit der Matrix multipliziert
     *              werden soll
     * @return Ein neues Rechteck, das das Ergebnis der
     * Multiplikation repräsentiert
     * @see java.awt.Rectangle
     */
    public java.awt.Rectangle multiply(java.awt.Rectangle _rect);

    /**
     * Multipliziert ein Polygon mit der Matrix und liefert das
     * Ergebnis der Multiplikation zurück
     *
     * @param _poly Das Polygon, das mit der Matrix multipliziert
     *              werden soll
     * @return Ein neues Polygon, das das Ergebnis der
     * Multiplikation repräsentiert
     * @see java.awt.Polygon
     */
    public java.awt.Polygon multiply(java.awt.Polygon _poly);


}
