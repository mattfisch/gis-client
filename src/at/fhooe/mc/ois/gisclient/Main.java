package at.fhooe.mc.ois.gisclient;

import at.fhooe.mc.ois.gisclient.controller.DrawingViewController;
import at.fhooe.mc.ois.gisclient.controller.MainViewController;
import at.fhooe.mc.ois.gisclient.controller.ManageViewController;
import at.fhooe.mc.ois.gisclient.model.GISModel;
import at.fhooe.mc.ois.gisclient.view.DrawingView;
import at.fhooe.mc.ois.gisclient.view.MainFrame;
import at.fhooe.mc.ois.gisclient.view.ManageView;

/**
 * The main entry point of the application. Sets up all the views, controller and the model and
 * sets the connection between them.
 */
public class Main {
    public static void main(String[] _args) {

        //initialize the model
        GISModel model = new GISModel();

        //initializing Views
        DrawingView drawingView = new DrawingView(new DrawingViewController(model));
        ManageView manageView = new ManageView(new ManageViewController(model));

        //add observer to model
        model.addObserver(drawingView);

        //starts application
        new MainFrame(new MainViewController(model), drawingView, manageView);
    }
}
