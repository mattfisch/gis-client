package at.fhooe.mc.ois.gisclient.model;

import at.fhooe.mc.ois.gisclient.entity.geodoublepoint.GeoDoublePoint;
import at.fhooe.mc.ois.gisclient.entity.geoobject.GeoObjectPart;
import at.fhooe.mc.ois.gisclient.entity.geoobject.POIObject;
import at.fhooe.mc.ois.gisclient.util.DrawingPanel;
import at.fhooe.mc.ois.gisclient.entity.geoobject.GeoObject;
import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;
import at.fhooe.mc.ois.gisclient.util.server.Server;
import at.fhooe.mc.ois.gisclient.util.server.ServerManager;
import at.fhooe.mc.ois.gisclient.view.ManageView;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/**
 * The Model Class, which does all the heavy lifting. This class generats the graphics, which will be drawn to the
 * DrawingView.
 */
public class GISModel implements IGISModel {

    /**
     * The models scale changed callback
     */
    private ManageView.OnScaleFactorChanged mOnScaleFactorChanged;

    /**
     * current house height, set by the awt framework
     */
    private int mWindowHeight;

    /**
     * current house width, set by the awt framework
     */
    private int mWindowWidth;

    /**
     * All current subscribed observers
     */
    private List<DrawingPanelObserver> mObserverList;

    /**
     * the objects, which will be displayed on the screen
     */
    private Vector<GeoObject> mWorldCoordinates;

    /**
     * Vector containing all POIs, which should be drawn on the map
     */
    private Vector<POIObject> mPOIs;

    /**
     * The transformations, which are currently applied to the object on screen
     */
    private Matrix mTransformationMatrix;

    /**
     * The drawing panel, into which the map is drawn
     */
    private DrawingPanel mCurrentDrawingPanel;

    /**
     * The models current scale to the world coordinates
     */
    private int mCurrentScale;

    /**
     * The type of the server, which is currently used by the model
     */
    private ServerManager.SERVER_TYPE mCurrentServer;

    /**
     * Observer-Pattern like DrawingPanedObserver for the DrawingPanel's changes.
     *
     * @see DrawingPanel
     */
    public interface DrawingPanelObserver {
        void update(DrawingPanel _data);
    }

    /**
     * Initializes a new instance of the GISModel class.
     */
    public GISModel() {
        mObserverList = new LinkedList<>();
        mTransformationMatrix = Matrix.getIdentityMatrix();
        mWorldCoordinates = new Vector<>();

        mCurrentServer = ServerManager.SERVER_TYPE.DUMMY;
    }

    /**
     * Sets the callback to the model
     *
     * @param _onScaleFactorChanged the callback
     */
    public void setOnScaleFactorChangedCallback(ManageView.OnScaleFactorChanged _onScaleFactorChanged) {
        mOnScaleFactorChanged = _onScaleFactorChanged;
    }

    /**
     * Called by the controller, when the user changed the server, from which to load the geo data
     *
     * @param _type the choosen server type
     * @see ServerManager.SERVER_TYPE
     */
    public void serverChanged(ServerManager.SERVER_TYPE _type) {
        mCurrentServer = _type;
    }

    /**
     * Loads the data from the currently selected GIS-Server
     */
    public void loadData() {
        mWorldCoordinates.clear();

        Server server = ServerManager.getInstance(mCurrentServer);
        if (server.openConnection()) {
            System.out.println("CONNECTED!");
        } else {
            System.out.println("FAILED TO CONNECT!");
        }

        server.addObserver(new Server.ServerObserver() {
            @Override
            public void onNewDataAvailable(GeoObject _object) {
                mWorldCoordinates.add(_object);
            }

            @Override
            public void onFetchingDataDone() {
                System.out.println("JOBS' DONE!");
                mTransformationMatrix = Matrix.getIdentityMatrix();
                redrawModel();
                zoomToFit();
            }

            @Override
            public void onError(String _error) {
                System.out.println("SERVER ERROR with message: " + _error);
            }
        });

        switch (mCurrentServer) {
            case DUMMY: {
                mPOIs = new Vector<>();
                mPOIs.add(new POIObject(getClass().getResource("/icons/church.png"), new Point(54056610, 580440736)));
                mPOIs.add(new POIObject(getClass().getResource("/icons/hospital.png"), new Point(54011307, 580446645)));
                mPOIs.add(new POIObject(getClass().getResource("/icons/parking.png"), new Point(54019418, 580462287)));
                mPOIs.add(new POIObject(getClass().getResource("/icons/police.png"), new Point(54049079, 580462171)));
                mPOIs.add(new POIObject(getClass().getResource("/icons/shopping.png"), new Point(54041895, 580448615)));
                server.setQuery("„SELECT * FROM data WHERE type in (233, 931, 932, 933, 934, 1101)");
                server.getData();
            }
            break;
            case POSTGRE: {
                server.setQuery("SELECT * FROM osm_boundary WHERE type in (8002)");
                server.getData();

                server.setQuery("SELECT * FROM osm_natural WHERE type in (6001,6002,6003,6004,6005,6006,6007," +
                        "6008,6009,6010,6011,6012,6013,6014,6015)");
                server.getData();

                server.setQuery("SELECT * FROM osm_landuse WHERE type in (5004,5006)");
                server.getData();

                server.setQuery("SELECT * FROM osm_highway WHERE type in (1030,1031,1040,1041,1050,1051,1090,1091)");
                server.getData();

                server.setQuery("SELECT * FROM osm_building");
                server.getData();
            }
        }
    }

    /**
     * Loads only the currently on the screen seen geo data from the postgis server
     */
    public void loadSticky() {
        mWorldCoordinates.clear();

        Rectangle bbox = getWindowBoundsInWorldCoordinates();

        if (mCurrentServer == ServerManager.SERVER_TYPE.POSTGRE) {
            Server server = ServerManager.getInstance(ServerManager.SERVER_TYPE.POSTGRE);

            String query1 = "SELECT * FROM osm_boundary " + "AS box WHERE box.geom && ST_MakeEnvelope("
                    + bbox.getMinX() + "," + bbox.getMinY() + "," + bbox.getMaxX() + "," + bbox.getMaxY() + ")"
                    + " AND type IN (8002)";

            String query2 = "SELECT * FROM osm_natural " + "AS box WHERE box.geom && ST_MakeEnvelope("
                    + bbox.getMinX() + "," + bbox.getMinY() + "," + bbox.getMaxX() + "," + bbox.getMaxY() + ")"
                    + " AND type IN (6001,6002,6005)";

            String query3 = "SELECT * FROM osm_landuse " + "AS box WHERE box.geom && ST_MakeEnvelope("
                    + bbox.getMinX() + "," + bbox.getMinY() + "," + bbox.getMaxX() + "," + bbox.getMaxY() + ")"
                    + " AND type IN (5004,5006)";

            String query4 = "SELECT * FROM osm_building " + "AS box WHERE box.geom && ST_MakeEnvelope("
                    + bbox.getMinX() + "," + bbox.getMinY() + "," + bbox.getMaxX() + "," + bbox.getMaxY() + ")";

            server.setQuery(query1);
            server.getData();

            server.setQuery(query2);
            server.getData();

            server.setQuery(query3);
            server.getData();

            server.setQuery(query4);
            server.getData();
        }
    }

    /**
     * Redraws everything, which is currently in the WordlCoordinates list
     */
    private void redrawModel() {
        mCurrentDrawingPanel = new DrawingPanel(mWindowWidth, mWindowHeight);
        mCurrentDrawingPanel.drawToContainer(mWorldCoordinates, mTransformationMatrix);
        updateObservers(mCurrentDrawingPanel);
    }

    /**
     * Saves the currently seen map on the drawing panel into a jpg
     */
    public void saveMap() {
        if (mCurrentDrawingPanel != null) {
            try {
                // retrieve image
                BufferedImage bi = mCurrentDrawingPanel;
                File outputfile = new File("GIS_CLIENT_MAP.png");
                ImageIO.write(bi, "png", outputfile);
            } catch (IOException e) {
                System.out.println("Writing image failed");
            }
        }
    }

    /**
     * Updates the dimensions, the drawing window uses on screen.
     *
     * @param _width  the new width used by the drawing window.
     * @param _height the new height used by the drawing window.
     */
    public void updateDrawingWindowDimensions(int _width, int _height) {
        mWindowHeight = _height;
        mWindowWidth = _width;
        redrawModel();
    }

    /**
     * Adds an observer to the list of observers, which will be notified, when the current home changes in the model
     *
     * @param _observer a class, which wants to be notified, if something changes
     * @see DrawingPanelObserver
     */
    public void addObserver(DrawingPanelObserver _observer) {
        mObserverList.add(_observer);
    }

    /**
     * Removes an observer from the list of observers
     *
     * @param _observer the observer, which will be removed from the subscribed observers
     * @see DrawingPanelObserver
     */
    public void removeObserver(DrawingPanelObserver _observer) {
        mObserverList.remove(_observer);
    }

    /**
     * Notifies all subscribers about changes in the model.
     *
     * @param _data the changed entity
     * @see DrawingPanelObserver
     * @see DrawingPanel
     */
    private void updateObservers(DrawingPanel _data) {
        mObserverList.forEach(_dataObserver -> _dataObserver.update(_data));
    }

    /**
     * Berechnet den gerade sichtbaren Massstab der Karte
     *
     * @return der Darstellungsmassstab
     * @see Matrix
     */
    private void calculateScale() {
        GeoDoublePoint vector = new GeoDoublePoint(0, 1.0);
        GeoDoublePoint vector_transformed = mTransformationMatrix.multiply(vector);
        double dpiCm = 72 / 2.54;
        mCurrentScale = (int) ((1 / vector_transformed.length()) * dpiCm);
        mOnScaleFactorChanged.scaleFactor(mCurrentScale);
    }

    /**
     * sets the models zoom to the scale factor
     *
     * @param _scale the scale factor to which zoom to
     */
    public void zoomToScale(Double _scale) {
        if (mCurrentScale >= 0)
            zoom(mCurrentScale / _scale);
    }

    /**
     * Shows the POIS on the map
     *
     * @param _show if show is true, the pois are shown, else, they are hidden
     */
    public void togglePois(Boolean _show) {
        if (_show) {
            mWorldCoordinates.addAll(mPOIs);
        } else {
            mWorldCoordinates.removeAll(mPOIs);
        }
        redrawModel();
    }

    /**
     * Transforms the current window into the same window in world coordinates
     *
     * @return the window as rectangle in world coordinates
     */
    private Rectangle getWindowBoundsInWorldCoordinates() {
        Rectangle rectangle = new Rectangle(getMapPoint(new Point(0, 0)));
        rectangle.add(getMapPoint(new Point(mWindowWidth, mWindowHeight)));
        return rectangle;
    }

    @Override
    public void zoomToFit() {
        Rectangle worldBounds = getMapBounds(mWorldCoordinates);

        //To see the lines of the most outside parts of the polygons, we need to reduce the size of the rectangle, into
        //which will be drawn by exactly one pixel in height and width
        Rectangle windowBounds = new Rectangle(mWindowWidth - 1, mWindowHeight - 1);

        mTransformationMatrix = Matrix.zoomToFit(worldBounds, windowBounds);
        redrawModel();

        calculateScale();
    }

    @Override
    public void zoom(double _factor) {
        zoom(new Point(mWindowWidth / 2, mWindowHeight / 2), _factor);
        calculateScale();
    }

    @Override
    public void zoom(Point _pt, double _factor) {
        mTransformationMatrix = Matrix.zoomPoint(mTransformationMatrix, _pt, _factor);
        redrawModel();
        calculateScale();
    }

    @Override
    public Rectangle getMapBounds(Vector<GeoObject> _geoObjects) {
        Rectangle outerBounds = null;
        for (GeoObject geoObject : _geoObjects) {
            outerBounds = (outerBounds == null) ? geoObject.getBounds() : outerBounds.union(geoObject.getBounds());
        }
        return outerBounds;
    }

    @Override
    public void scrollHorizontal(int _delta) {
        mTransformationMatrix = Matrix.translate(_delta, 0).multiply(mTransformationMatrix);
        redrawModel();
    }

    @Override
    public void scrollVertical(int _delta) {
        mTransformationMatrix = Matrix.translate(0, _delta).multiply(mTransformationMatrix);
        redrawModel();
    }

    @Override
    public void rotateLeft(double _alpha) {
        mTransformationMatrix =
                Matrix.translate(mWindowWidth / 2, mWindowHeight / 2)
                        .multiply(Matrix.rotate(-_alpha)
                                .multiply(Matrix.translate(-mWindowWidth / 2, -mWindowHeight / 2))
                                .multiply(mTransformationMatrix));
        redrawModel();
    }

    @Override
    public void rotateRight(double _alpha) {
        rotateLeft(-_alpha);
    }

    @Override
    public Vector<GeoObject> initSelection(Point _pt) {
        Vector<GeoObject> containsPoint = new Vector<>();
        Point worldPoint = getMapPoint(_pt);
        mWorldCoordinates.forEach(_geoObject -> {
            if (_geoObject.getBounds().contains(worldPoint))
                for (GeoObjectPart part : _geoObject.getGeoObjectParts()) {
                    if (part.contains(worldPoint))
                        containsPoint.add(_geoObject);
                }
        });
        return containsPoint;
    }

    @Override
    public void zoomRect(Rectangle _mapBounds) {

        System.out.println(_mapBounds.toString());

        Rectangle worldBounds = Matrix.mirrorX().multiply(Matrix.mirrorY()).multiply(mTransformationMatrix.invers().multiply(_mapBounds));

        //To see the lines of the most outside parts of the polygons, we need to reduce the size of the rectangle, into
        //which will be drawn by exactly one pixel in height and width

        Rectangle windowBounds = new Rectangle(mWindowWidth - 1, mWindowHeight - 1);
        mTransformationMatrix = Matrix.zoomToFit(worldBounds, windowBounds);

        System.out.println(worldBounds.toString());
        System.out.println(windowBounds.toString());


        redrawModel();
        calculateScale();
    }

    @Override
    public Point getMapPoint(Point _pt) {
        return Matrix.mirrorX().multiply(Matrix.mirrorY()).multiply(mTransformationMatrix.invers().multiply(_pt));
    }
}
