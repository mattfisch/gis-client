package at.fhooe.mc.ois.gisclient.model;

import at.fhooe.mc.ois.gisclient.entity.geoobject.GeoObject;

import java.awt.*;
import java.util.Vector;

/**
 * This interface represents the model
 */
public interface IGISModel {
    /**
     * Stellt intern eine Transformationsmatrix zur Verfuegung, die so
     * skaliert, verschiebt und spiegelt, dass die zu zeichnenden Polygone
     * komplett in den Anzeigebereich passen
     */
    public void zoomToFit();

    /**
     * Veraendert die interne Transformationsmatrix so, dass in das
     * Zentrum des Anzeigebereiches herein- bzw. herausgezoomt wird
     *
     * @param _factor Der Faktor um den herein- bzw. herausgezoomt wird
     */
    public void zoom(double _factor);

    /**
     * Veraendert die interne Transformationsmatrix so, dass an dem
     * uebergebenen Punkt herein- bzw. herausgezoomt wird
     *
     * @param _pt     Der Punkt an dem herein- bzw. herausgezoomt wird
     * @param _factor Der Faktor um den herein- bzw. herausgezoomt wird
     */
    public void zoom(Point _pt, double _factor);

    /**
     * Ermittelt die gemeinsame BoundingBox der uebergebenen Polygone
     *
     * @param _geoObjects Die GeoObjects, fuer die die BoundingBox berechnet
     *                    werden soll
     * @return Die BoundingBox
     */
    public Rectangle getMapBounds(Vector<GeoObject> _geoObjects);

    /**
     * Veraendert die interne Transformationsmatrix so, dass
     * die zu zeichnenden Objekt horizontal verschoben werden.
     *
     * @param _delta Die Strecke, um die verschoben werden soll
     */
    public void scrollHorizontal(int _delta);

    /**
     * Veraendert die interne Transformationsmatrix so, dass
     * die zu zeichnenden Objekte horizontal verschoben werden.
     *
     * @param _delta Die Strecke, um die verschoben werden soll
     */
    public void scrollVertical(int _delta);

    /**
     * Veraendert die interne Transformationsmatrix so, dass
     * die zu zeichnenden Objekte nach links rotiert werden.
     *
     * @param _alpha Der Winkel, um den rotiert werden soll
     */
    public void rotateLeft(double _alpha);

    /**
     * Veraendert die interne Transformationsmatrix so, dass
     * die zu zeichnenden Objekte nach rechts rotiert werden.
     *
     * @param _alpha Der Winkel, um den rotiert werden soll
     */
    public void rotateRight(double _alpha);

    /**
     * Ermittelt die Geo-Objekte, die den Punkt (in Bildschirmkoordinaten)
     * enthalten
     *
     * @param _pt Ein Selektionspunkt im Bildschirmkoordinatensystem
     * @return Ein Vektor von Geo-Objekte, die den Punkt enthalten
     * @see java.awt.Point
     * @see GeoObject
     */
    public Vector<GeoObject> initSelection(Point _pt);

    /**
     * Stellt intern eine Transformationsmatrix zur Verfuegung, die so
     * skaliert, verschiebt und spiegelt, dass die zu zeichnenden Polygone
     * innerhalb eines definierten Rechtecks (_mapBounds) komplett in den
     * Anzeigebereich (die Zeichenflaeche) passen
     *
     * @param _mapBounds Der darzustellende Bereich in Welt-Koordinaten
     */
    public void zoomRect(Rectangle _mapBounds);

    /**
     * Liefert zu einem Punkt im Bildschirmkoordinatensystem den passenden
     * Punkt im Kartenkoordinatensystem
     *
     * @param _pt Der umzuwandelnde Punkt im Bildschirmkoordinatensystem
     * @return Der gleiche Punkt im Weltkoordinatensystem
     * @see java.awt.Point
     */
    public Point getMapPoint(Point _pt);
}
