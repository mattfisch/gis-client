package at.fhooe.mc.ois.gisclient.controller;

import at.fhooe.mc.ois.gisclient.model.GISModel;
import at.fhooe.mc.ois.gisclient.util.server.ServerManager;
import at.fhooe.mc.ois.gisclient.view.ManageView;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * MainViewController manages all events, which happen on the applications (MainFrame class).
 */
public class MainViewController implements WindowListener {

    /**
     * The model, which does all the heavy lifting in the background
     */
    private GISModel mModel;

    /**
     * Initializes the MainViewController
     *
     * @param _model set the model, to which the controller talks
     */
    public MainViewController(GISModel _model) {
        mModel = _model;
    }

    @Override
    public void windowOpened(WindowEvent _e) {

    }

    @Override
    public void windowClosing(WindowEvent _e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent _e) {

    }

    @Override
    public void windowIconified(WindowEvent _e) {

    }

    @Override
    public void windowDeiconified(WindowEvent _e) {

    }

    @Override
    public void windowActivated(WindowEvent _e) {

    }

    @Override
    public void windowDeactivated(WindowEvent _e) {

    }

    public void onServerMenuClicked(ServerManager.SERVER_TYPE _type) {
        mModel.serverChanged(_type);
    }
}
