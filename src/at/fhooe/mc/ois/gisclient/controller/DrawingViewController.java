package at.fhooe.mc.ois.gisclient.controller;

import at.fhooe.mc.ois.gisclient.entity.geoobject.GeoObject;
import at.fhooe.mc.ois.gisclient.model.GISModel;
import at.fhooe.mc.ois.gisclient.view.DetailsDialogView;
import at.fhooe.mc.ois.gisclient.view.DrawingView;
import javafx.scene.input.MouseButton;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.*;
import java.io.IOException;
import java.util.Vector;

/**
 * DrawingViewController manages all events, which happen on the drawing panel (DrawingView class).
 */
public class DrawingViewController implements ComponentListener, MouseListener, MouseMotionListener, MouseWheelListener {

    /**
     * The model, which does all the heavy lifting in the background
     */
    private GISModel mModel;

    /**
     * The controllers view
     */
    private DrawingView mDrawingView;

    /**
     * The point where the right mouse was pressed
     */
    private Point mMouseRightPressedAt;

    /**
     * The point, where the right mouse was pressed
     */
    private Point mMouseLeftPressedAt;

    /**
     * The point, where the right mouse was last dragged
     */
    private Point mMouseRightLastDragged;

    /**
     * Currently selected rectangle, into this will be zoomed into
     */
    private Rectangle mUserSelection;

    /**
     * Initializes the DrawingViewController
     *
     * @param _model the Model, with which the controller communicates
     */
    public DrawingViewController(GISModel _model) {
        mModel = _model;
    }

    /**
     * Sets the drawing view, to which this controller corresponds
     *
     * @param _drawingView the drawing view of this controller
     */
    public void setDrawingView(DrawingView _drawingView) {
        mDrawingView = _drawingView;
    }

    /**
     * Clears the clipboard and adds a single point to is
     *
     * @param _point the point, as string, which should be added
     */
    private void copyToClipboard(String _point) {
        StringSelection selection = new StringSelection(_point);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
        System.out.println(_point.toString());
    }

    /**
     * Adds the point (as string) to the current clipboard values
     *
     * @param _point the point, as string, which should be added
     */
    private void addToClipboard(String _point) {
        String oldPoints = null;
        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        // set data flavor we want to get from the clipboard
        DataFlavor dataFlavor = DataFlavor.stringFlavor;
        // get content of clipboard if available
        if (systemClipboard.isDataFlavorAvailable(dataFlavor)) {
            try { // save it to string
                oldPoints = (String) systemClipboard.getData(dataFlavor);
            } catch (UnsupportedFlavorException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // save old and new points to clipboard
            copyToClipboard(oldPoints + _point);
        }
    }

    @Override
    public void componentResized(ComponentEvent _e) {
        int height = (int) _e.getComponent().getSize().getHeight();
        int width = (int) _e.getComponent().getSize().getWidth();
        mModel.updateDrawingWindowDimensions(width, height);
    }

    @Override
    public void componentMoved(ComponentEvent _e) {

    }

    @Override
    public void componentShown(ComponentEvent _e) {

    }

    @Override
    public void componentHidden(ComponentEvent _e) {

    }

    @Override
    public void mouseClicked(MouseEvent _e) {
        if (_e.getButton() == MouseEvent.BUTTON1) {
            Point point = mModel.getMapPoint(_e.getPoint());
            String pointStr = "(" + point.x + "," + point.y + ")\n";
            if (_e.isControlDown()) {
                addToClipboard(pointStr);
            } else {
                copyToClipboard(pointStr);
            }
        }

        if (_e.getClickCount() >= 2) {
            Vector<GeoObject> selectedItems = mModel.initSelection(_e.getPoint());
            if (!selectedItems.isEmpty())
                mDrawingView.showDialog(_e.getPoint(), selectedItems);
        }
    }

    @Override
    public void mousePressed(MouseEvent _e) {
        if (_e.getButton() == MouseEvent.BUTTON3) {
            mDrawingView.setCursor(new Cursor(Cursor.HAND_CURSOR));
            mMouseRightPressedAt = new Point(_e.getX(), _e.getY());
        }

        if (_e.getButton() == MouseEvent.BUTTON1) {
            mMouseLeftPressedAt = _e.getPoint();
            if (_e.getClickCount() == 1)
                mDrawingView.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        }
    }

    @Override
    public void mouseReleased(MouseEvent _e) {
        if (_e.getButton() == MouseEvent.BUTTON3) {
            if (mMouseRightPressedAt != null) {
                int deltaX = mMouseRightLastDragged.x - mMouseRightPressedAt.x;
                int deltaY = mMouseRightLastDragged.y - mMouseRightPressedAt.y;
                mModel.scrollVertical(deltaY);
                mModel.scrollHorizontal(deltaX);
            }
        }

        if (_e.getButton() == MouseEvent.BUTTON1) {
            if (mUserSelection != null)
                mDrawingView.drawUserSelection(mUserSelection);

            Rectangle mapBound = new Rectangle(mMouseLeftPressedAt);
            mapBound.add(_e.getPoint());

            if (mapBound.getWidth() > 20 && mapBound.getHeight() > 20)
                mModel.zoomRect(mapBound);

            mUserSelection = null;
        }

        mDrawingView.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    @Override
    public void mouseEntered(MouseEvent _e) {

    }

    @Override
    public void mouseExited(MouseEvent _e) {

    }

    @Override
    public void mouseDragged(MouseEvent _e) {
        if (_e.getButton() == MouseEvent.BUTTON1) {
            if (mUserSelection != null)
                mDrawingView.drawUserSelection(mUserSelection);
            mUserSelection = new Rectangle(_e.getPoint());
            mUserSelection.add(mMouseLeftPressedAt);
            mDrawingView.drawUserSelection(mUserSelection);
        }
        if (_e.getButton() == MouseEvent.BUTTON3) {
            if (mMouseRightLastDragged == null)
                mDrawingView.animateDrag(mMouseRightPressedAt, _e.getPoint());
            else
                mDrawingView.animateDrag(mMouseRightLastDragged, _e.getPoint());

        }
        mMouseRightLastDragged = _e.getPoint();
    }

    @Override
    public void mouseMoved(MouseEvent _e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent _e) {
        int notches = _e.getWheelRotation();
        if (notches < 0) {
            mModel.zoom(1.3 * -notches);
        } else {
            mModel.zoom((1 / 1.3) * notches);
        }
    }
}
