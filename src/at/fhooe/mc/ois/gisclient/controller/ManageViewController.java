package at.fhooe.mc.ois.gisclient.controller;

import at.fhooe.mc.ois.gisclient.model.GISModel;
import at.fhooe.mc.ois.gisclient.view.ManageView;

/**
 * ManageViewController manages all events, which happen on the buttons in the manage view (ManageView class).
 */
public class ManageViewController {

    /**
     * The model, which does all the heavy lifting in the background
     */
    private GISModel mModel;

    /**
     * Initializes the ManageViewController
     *
     * @param _model the Model, with which the controller communicates
     */
    public ManageViewController(GISModel _model) {
        mModel = _model;
    }

    /**
     * Sets a callback, linking the models scale factor data change to the views, textview
     *
     * @param _scaleFactorCallback callback to the view, containing the textview
     */
    public void setScaleFactorCallback(ManageView.OnScaleFactorChanged _scaleFactorCallback) {
        mModel.setOnScaleFactorChangedCallback(_scaleFactorCallback);
    }

    /**
     * Draws the model on the display, when clicked
     */
    public void onLoadClicked() {
        mModel.loadData();
    }

    /**
     * Calls the model, to zoom all the geo objects to fit into the screen
     */
    public void onZoomToFitClicked() {
        mModel.zoomToFit();
    }

    /**
     * Calls the model, to rotate its contents to the left
     */
    public void onRotateLeftClicked() {
        mModel.rotateLeft(5 * Math.PI / 180);
    }

    /**
     * Calls the model, to rotate its contents to the right
     */
    public void onRotateRightClicked() {
        mModel.rotateRight(5 * Math.PI / 180);
    }

    /**
     * Calls the model, to zoom into the drawing view
     */
    public void onZoomInClicked() {
        mModel.zoom(1.3);
    }

    /**
     * Calls the model, to zoom out of the drawing view
     */
    public void onZoomOutClicked() {
        mModel.zoom(1 / 1.3);
    }

    /**
     * Calls the model, to move to the top
     */
    public void onMoveNorthClicked() {
        mModel.scrollVertical(-20);
    }

    /**
     * Calls the model, to move to the right
     */
    public void onMoveEastClicked() {
        mModel.scrollHorizontal(20);
    }

    /**
     * Calls the model, to move to the bottom
     */
    public void onMoveSoutClicked() {
        mModel.scrollVertical(20);
    }

    /**
     * Calls the model, to move to the left
     */
    public void onMoveWestClicked() {
        mModel.scrollHorizontal(-20);
    }

    /**
     * Sets the scale to the model
     *
     * @param _scale the scale, which the user wanted
     */
    public void setScale(Double _scale) {
        mModel.zoomToScale(_scale);
    }

    /**
     * Tells the Modle to show or hide the POIS
     *
     * @param _show true, if the POIS should be shown, false otherwise
     */
    public void togglePois(Boolean _show) {
        mModel.togglePois(_show);
    }

    /**
     * Calls the model to change the mapdata shown to sticky mode, meaning only the currently visible things are shown
     */
    public void setSticky() {
        mModel.loadSticky();
    }

    /**
     * Saves the currently shown map as jpg in the projects source dir
     */
    public void saveMap() {
        mModel.saveMap();
    }
}
