package at.fhooe.mc.ois.gisclient.view;

import at.fhooe.mc.ois.gisclient.controller.MainViewController;
import at.fhooe.mc.ois.gisclient.util.server.ServerManager;

import java.awt.*;

/**
 * The MainFrame class extends the AWT.{@link Frame} class and therefore represents the outer most
 * window of the running application
 */
public class MainFrame extends Frame {

    public static final int WINDOW_WIDTH = 800;

    /**
     * The views corresponding controller
     */
    MainViewController mMainViewController;

    /**
     * Initializes the main window and when done, shows it on the users screen
     *
     * @param _controller  the views corresponding controller
     * @param _drawingView the drawingview, which is within the MainFrame
     * @param _manageView  the manageview, which is within the MainFrame
     * @see MainViewController
     * @see DrawingView
     * @see ManageView
     */
    public MainFrame(MainViewController _controller, DrawingView _drawingView, ManageView _manageView) {
        super();
        mMainViewController = _controller;

        addWindowListener(mMainViewController);

        setSize(WINDOW_WIDTH, 900);

        //build UI
        setLayout(new BorderLayout());
        add(_drawingView, BorderLayout.CENTER);
        add(_manageView, BorderLayout.SOUTH);

        _drawingView.setContainingWindow(this);

        MenuBar bar = new MenuBar();

        Menu server = new Menu("SERVER");

        MenuItem item = new MenuItem("DUMMY");
        item.addActionListener(_e -> mMainViewController.onServerMenuClicked(ServerManager.SERVER_TYPE.DUMMY));
        MenuItem item1 = new MenuItem("PostgreSql");
        item1.addActionListener(_e -> mMainViewController.onServerMenuClicked(ServerManager.SERVER_TYPE.POSTGRE));

        server.add(item);
        server.add(item1);

        bar.add(server);
        setMenuBar(bar);

        //show UI
        setVisible(true);
    }
}
