package at.fhooe.mc.ois.gisclient.view;

import at.fhooe.mc.ois.gisclient.entity.geoobject.GeoObject;
import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

import static at.fhooe.mc.ois.gisclient.view.MainFrame.WINDOW_WIDTH;

public class DetailsDialogView extends Dialog {

    /**
     * Contains all the objects, which will be displayed inside of the dialog
     */
    private final Vector<GeoObject> mGeoObjects;

    /**
     * Following members purpose is displaying an Geo Objects information
     */
    private TextField mTextFieldId;

    /**
     * The type of the geo object
     */
    private TextField mTextFieldType;

    /**
     * The geometry of the geo object will be displayed here
     */
    private TextField mTextFieldGeometryType;

    /**
     * The attributes of the geo object as string will be printed here
     */
    private TextArea mDetailsText;

    /**
     * Used to draw the geo object in the dialog window
     */
    DrawingPanel mDrawingPanel;

    /**
     * Constructor
     *
     * @param parent   the frame into which this dialog will be drawn
     * @param _source  the source point, where the view was clicked, at this point the dialog is shown
     * @param _objects a vector containing the geo objects at the clicked location
     */
    public DetailsDialogView(Frame parent, Point _source, Vector<GeoObject> _objects) {
        super(parent, true);

        mGeoObjects = _objects;

        //show dialog, where the mouse was clicked
        setLocation(_source);

        //dialog should have a white background
        setBackground(Color.LIGHT_GRAY);

        setSize(WINDOW_WIDTH, 500);

        setLayout(new BorderLayout());

        //bottom part
        Button closeDialog = new Button("Close");
        add(closeDialog, BorderLayout.SOUTH);

        closeDialog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        //left part
        List list = new List(mGeoObjects.size());
        mGeoObjects.forEach(_geoObject -> list.add(_geoObject.getId()));
        add(list, BorderLayout.WEST);

        list.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED)
                changeSelectedItem(mGeoObjects.get(list.getSelectedIndex()));
        });

        Panel rightPanel = new Panel(new GridLayout(3, 1));

        //right top part
        Panel rightTopPanel = new Panel(new GridLayout(3, 2));

        Label labelId = new Label("object id");
        mTextFieldId = new TextField();
        Label labelType = new Label("object type");
        mTextFieldType = new TextField();
        Label labelGeometryType = new Label("object geometry type");
        mTextFieldGeometryType = new TextField();
        rightTopPanel.add(labelId);
        rightTopPanel.add(mTextFieldId);
        rightTopPanel.add(labelType);
        rightTopPanel.add(mTextFieldType);
        rightTopPanel.add(labelGeometryType);
        rightTopPanel.add(mTextFieldGeometryType);

        //right center part
        Panel rightCenterPanel = new Panel();

        mDetailsText = new TextArea();
        mDetailsText.setEditable(false);

        rightCenterPanel.add(mDetailsText);

        //right bottom part
        mDrawingPanel = new DrawingPanel();
        mDrawingPanel.setSize(200, 200);
        mDrawingPanel.setBackground(Color.YELLOW);

        rightPanel.add(rightTopPanel);
        rightPanel.add(rightCenterPanel);
        rightPanel.add(mDrawingPanel);

        add(rightPanel);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                dispose();
            }
        });

        if (!mGeoObjects.isEmpty()) {
            list.select(0);
            changeSelectedItem(mGeoObjects.get(0));
        }
    }

    /**
     * Changes the selected item
     *
     * @param _geoObject the geo object, which is now selected
     */
    private void changeSelectedItem(GeoObject _geoObject) {
        mTextFieldId.setText(_geoObject.getId());
        mTextFieldType.setText(String.valueOf(_geoObject.getType()));
        mDetailsText.setText(parseAttributes(_geoObject.getAttributes()));
        mDrawingPanel.redrawGeoObject(_geoObject);
    }

    /**
     * parses the attribure string into a more readable form
     *
     * @param _attrs the attribute as string of the geo object
     * @return a more readable representation of the geo objects attribute string
     */
    private String parseAttributes(String _attrs) {
        if (_attrs != null) {
            StringBuilder stringBuilder = new StringBuilder(_attrs);

            //sets linebreaks
            while (stringBuilder.indexOf("|") != -1) {
                stringBuilder.replace(stringBuilder.indexOf("|"), stringBuilder.indexOf("|") + 1, "\n");
            }

            return stringBuilder.toString();
        } else {
            return "";
        }
    }

    /**
     * The panel, into which a representation of the geo object will be drawn
     */
    private class DrawingPanel extends Panel {

        /**
         * The geo object, which will be drawn into this panel
         */
        private GeoObject mGeoObject;

        /**
         * Constructor
         */
        public DrawingPanel() {
            super();
        }

        /**
         * Calls redraw, when a new geo object was selected
         *
         * @param _geoObject the new geo object, which will be drawn
         */
        private void redrawGeoObject(GeoObject _geoObject) {
            mGeoObject = _geoObject;
            repaint();
        }

        @Override
        public void paint(Graphics _g) {
            super.paint(_g);

            if (mGeoObject != null) {
                Rectangle windowBounds = new Rectangle(getWidth() - 1, getHeight() - 1);
                Matrix matrix = Matrix.zoomToFit(mGeoObject.getBounds(), windowBounds);
                mGeoObject.paint(_g, matrix);
            }
        }
    }
}
