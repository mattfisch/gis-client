package at.fhooe.mc.ois.gisclient.view;

import at.fhooe.mc.ois.gisclient.controller.DrawingViewController;
import at.fhooe.mc.ois.gisclient.entity.geoobject.GeoObject;
import at.fhooe.mc.ois.gisclient.util.DrawingPanel;
import at.fhooe.mc.ois.gisclient.model.GISModel;

import java.awt.*;
import java.util.Vector;

import static at.fhooe.mc.ois.gisclient.view.MainFrame.WINDOW_WIDTH;

/**
 * The view, which represents the canvas, on which will be drawn at runtime.
 */
public class DrawingView extends Panel implements GISModel.DrawingPanelObserver {

    /**
     * The default height of the drawing panel
     */
    public static final int DRAWINGVIEW_HEIGHT = 480;

    /**
     * The MainFrame's frame
     */
    private Frame mWindow;

    /**
     * An instance of the drawing container, which will be redrawn on the next repaint() call
     */
    private DrawingPanel mDrawingContainer;

    /**
     * The views corresponding controller
     */
    private DrawingViewController mDrawingViewController;

    /**
     * Initializes a new instance of the DrawingView
     *
     * @param _controller an instance of the DrawingVies corresponding controller
     * @see DrawingViewController
     */
    public DrawingView(DrawingViewController _controller) {
        super();
        setBackground(Color.WHITE);
        setSize(WINDOW_WIDTH, DRAWINGVIEW_HEIGHT);
        mDrawingViewController = _controller;
        _controller.setDrawingView(this);
        addComponentListener(mDrawingViewController);
        addMouseListener(mDrawingViewController);
        addMouseMotionListener(mDrawingViewController);
        addMouseWheelListener(mDrawingViewController);
    }

    /**
     * Sets the frame, which contains this panel
     *
     * @param parent the parent frame
     */
    public void setContainingWindow(Frame parent) {
        mWindow = parent;
    }

    /**
     * displays a dialog view into to parent frame of this panel
     *
     * @param _source the point, where the dialog should be drawn
     * @param _data   the geo objects, which details schould be displayed in the dialog
     * @return the dialog
     */
    public DetailsDialogView showDialog(Point _source, Vector<GeoObject> _data) {
        DetailsDialogView dialogView = new DetailsDialogView(mWindow, _source, _data);
        dialogView.setVisible(true);
        return dialogView;
    }

    /**
     * Draws a rectangle into this panel, representing the users current selection box
     *
     * @param _rectangle the rectangle, which should be drawn
     */
    public void drawUserSelection(Rectangle _rectangle) {
        Graphics g = getGraphics();
        g.setXORMode(Color.YELLOW);
        g.drawRect(_rectangle.x, _rectangle.y, _rectangle.width, _rectangle.height);
        g.setPaintMode();
    }

    /**
     * Animate the dragging of the panels content from one to another point
     *
     * @param _from the point where to start the dragging
     * @param _to   the point where to finish the dragging
     */
    public void animateDrag(Point _from, Point _to) {
        int deltaX = _to.x - _from.x;
        int deltaY = _to.y - _from.y;

        Graphics g = getGraphics();
        g.copyArea(0, 0, getWidth(), getHeight(), deltaX, deltaY);

        //upper rect
        g.clearRect(0, 0, getWidth(), 2);

        //lower rect
        g.clearRect(0, getHeight() - 2, getWidth(), 2);

        //left rect
        g.clearRect(0, 0, 2, getHeight());

        //right rect
        g.clearRect(getWidth() - 2, 0, 2, getHeight());

    }

    /**
     * Changes the applications currently used cursor, depending the users current mouse action
     *
     * @param _cursor the new cursor
     */
    public void setCursor(Cursor _cursor) {
        mWindow.setCursor(_cursor);
    }

    @Override
    public void update(DrawingPanel _image) {
        mDrawingContainer = _image;
        repaint();
    }

    @Override
    public void paint(Graphics _g) {
        super.paint(_g);
        if (mDrawingContainer != null)
            mDrawingContainer.draw(_g);
    }
}
