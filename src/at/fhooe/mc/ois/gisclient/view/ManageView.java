package at.fhooe.mc.ois.gisclient.view;

import at.fhooe.mc.ois.gisclient.controller.ManageViewController;
import javafx.scene.layout.Pane;

import java.awt.*;
import java.awt.event.*;

import static at.fhooe.mc.ois.gisclient.view.MainFrame.WINDOW_WIDTH;

/**
 * The ManageView class represents the bottom menu, containing all the buttons which are used for using the application.
 */
public class ManageView extends Panel {

    /**
     * Used to call the view, when the scaling factor changed in the model
     */
    public interface OnScaleFactorChanged {
        void scaleFactor(int _scaleFactor);
    }

    /**
     * The default height of the manager view
     */
    public static final int MANAGEVIEW_HEIGHT = 200;

    /**
     * The views corresponding controller
     */
    private ManageViewController mManageViewController;

    /**
     * Callback for scalfactorchanges
     */
    private OnScaleFactorChanged mOnScaleFactorChangedCallback;

    /**
     * Initializes a new instance of the ManageView class, which its corresponding View Controller
     *
     * @param _controller the viewcontroller of the ManageView class
     * @see ManageViewController
     */
    public ManageView(ManageViewController _controller) {
        super();
        mManageViewController = _controller;

        setSize(WINDOW_WIDTH, MANAGEVIEW_HEIGHT);
        setLayout(new FlowLayout());

        //button to draw stuff
        Button drawButton = new Button("load");
        drawButton.addActionListener(e -> {
            mManageViewController.onLoadClicked();
        });
        add(drawButton);

        Panel zoomToFitPanel = new Panel(new GridLayout(3, 1));
        TextField scaleTextField = new TextField("1 : unknown");
        mOnScaleFactorChangedCallback = _scaleFactor -> scaleTextField.setText("1 : " + _scaleFactor);
        _controller.setScaleFactorCallback(mOnScaleFactorChangedCallback);

        Button setScaleToModel = new Button("set scale");
        setScaleToModel.addActionListener(e -> {
            String scaleFactorStr = scaleTextField.getText();
            scaleFactorStr = scaleFactorStr.substring(scaleFactorStr.indexOf(':') + 2, scaleFactorStr.length());
            mManageViewController.setScale(Double.valueOf(scaleFactorStr));
        });

        Button zoomToFit = new Button("Zoom to fit");
        zoomToFit.addActionListener(e -> mManageViewController.onZoomToFitClicked());

        zoomToFitPanel.add(setScaleToModel);
        zoomToFitPanel.add(scaleTextField);
        zoomToFitPanel.add(zoomToFit);

        add(zoomToFitPanel);

        //add the rotate left and rotate right buttons to the management view
        Button rotateLeft = new Button("rotate L");
        rotateLeft.addActionListener(e ->

        {
            mManageViewController.onRotateLeftClicked();
        });
        Button rotateRight = new Button("rotate R");
        rotateRight.addActionListener(e ->

        {
            mManageViewController.onRotateRightClicked();
        });
        Panel rotatePanel = new Panel();

        rotatePanel.setLayout(new

                BorderLayout());
        rotatePanel.add(rotateLeft, BorderLayout.NORTH);
        rotatePanel.add(rotateRight, BorderLayout.SOUTH);

        add(rotatePanel);

        //add the zoom in and out buttons to the management view
        Button zoomIn = new Button("+");
        zoomIn.addActionListener(e ->

        {
            mManageViewController.onZoomInClicked();
        });
        Button zoomOut = new Button("-");
        zoomOut.addActionListener(e ->

        {
            mManageViewController.onZoomOutClicked();
        });
        Panel zoomPanel = new Panel();

        zoomPanel.setLayout(new BorderLayout());
        zoomPanel.add(zoomIn, BorderLayout.NORTH);
        zoomPanel.add(zoomOut, BorderLayout.SOUTH);

        add(zoomPanel);

        //add the navigation buttons to the management view
        Button upNavigation = new Button("UP");
        upNavigation.addActionListener(e ->

        {
            mManageViewController.onMoveNorthClicked();
        });
        Button leftNavigation = new Button("LEFT");
        leftNavigation.addActionListener(e ->

        {
            mManageViewController.onMoveWestClicked();
        });
        Button rightNavigation = new Button("RIGHT");
        rightNavigation.addActionListener(e ->

        {
            mManageViewController.onMoveEastClicked();
        });
        Button downNavigation = new Button("DOWN");
        downNavigation.addActionListener(e ->

        {
            mManageViewController.onMoveSoutClicked();
        });

        Panel navigationPanel = new Panel();
        navigationPanel.setLayout(new BorderLayout());
        navigationPanel.add(upNavigation, BorderLayout.NORTH);
        navigationPanel.add(leftNavigation, BorderLayout.WEST);
        navigationPanel.add(rightNavigation, BorderLayout.EAST);
        navigationPanel.add(downNavigation, BorderLayout.SOUTH);

        add(navigationPanel);

        Checkbox togglePois = new Checkbox("show POIS");
        togglePois.addItemListener(e -> mManageViewController.togglePois(e.getStateChange() == ItemEvent.SELECTED));

        add(togglePois);


        Button sticky = new Button("Sticky");
        sticky.addActionListener(_e -> mManageViewController.setSticky());
        Button save = new Button("Save");
        save.addActionListener(_e -> mManageViewController.saveMap());

        Panel stickySavePanel = new Panel(new BorderLayout());
        stickySavePanel.add(sticky, BorderLayout.NORTH);
        stickySavePanel.add(save, BorderLayout.SOUTH);

        add(stickySavePanel);

        setBackground(Color.DARK_GRAY);
    }
}
