package at.fhooe.mc.ois.test;

import at.fhooe.mc.ois.gisclient.util.matrix.Matrix;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;

/**
 * Created by Vortech on 06/04/2017.
 */
public class MatrixTests {

    private Matrix mMatrix1;
    private Matrix mMatrix2;
    private Matrix mMatrix3;
    private Matrix mMatrix4;

    private Point mPoint1;
    private Point mPoint2;

    @BeforeEach
    public void initMatrizes() {
        mMatrix1 = new Matrix(
                234, 543, 123,
                564, 123, 543,
                876, 345, 678);
        mMatrix2 = new Matrix(
                23, 65, 87,
                23, 65, 23,
                76, 12, 99);
        mMatrix3 = new Matrix(
                1, 7, 4,
                9, 7, 3,
                5, 8, 7);
        mMatrix4 = new Matrix(
                123454, 32456, 98765,
                65463, 876546, 987634,
                234654, 87656, 234654);

        mPoint1 = new Point(55, 44);
        mPoint2 = new Point(23, 66);
    }

    @Test
    public void testPointMultiplication() {
        Point result1 = new Point(8316799, 43156123);
        Point result1Calculated = mMatrix4.multiply(mPoint1);
        Assertions.assertTrue(result1.equals(result1Calculated));


        Point result2 = new Point(5080303, 60345319);
        Point result2Calculated = mMatrix4.multiply(mPoint2);
        Assertions.assertTrue(result2.equals(result2Calculated));
    }

    @Test
    public void testMatrixMultiplication() {
        Matrix result1 = new Matrix(
                27219, 51981, 45024,
                57069, 51171, 105654,
                79611, 87501, 151269);
        Matrix result1Calculated = mMatrix1.multiply(mMatrix2);
        Assertions.assertTrue(result1.equals(result1Calculated));

        Matrix result2 = new Matrix(
                909383, 1881490, 10368919,
                12892547, 14495135, 100667256,
                2196828, 4133402, 24432330);
        Matrix result2Calculated = mMatrix4.multiply(mMatrix3);
        Assertions.assertTrue(result2.equals(result2Calculated));
    }

    @Test
    public void testRectangleMultiplication() {
        Rectangle rect = new Rectangle(100, 100);
        Rectangle scaledRect = Matrix.scale(1.5).multiply(rect);

        //after multiplying with the scale matrix with a scalefactor of 150, the result has 150 x 150 dimensions
        Rectangle resultRect = new Rectangle(150, 150);

        Assertions.assertTrue(resultRect.width == scaledRect.width);
        Assertions.assertTrue(resultRect.height == scaledRect.height);
    }

    @Test
    public void testPolygonMultiplication() {
        Polygon polygon = new Polygon();
        polygon.addPoint(0, 0);
        polygon.addPoint(100, 0);
        polygon.addPoint(100, 100);
        polygon.addPoint(0, 100);

        Polygon scaledPoly = Matrix.scale(1.5).multiply(polygon);

        //after multiplying with the scale matrix with a scalefactor of 150, the result has 150 x 150 dimensions
        Polygon resultPoly = new Polygon();
        resultPoly.addPoint(0, 0);
        resultPoly.addPoint(150, 0);
        resultPoly.addPoint(150, 150);
        resultPoly.addPoint(0, 150);

        Rectangle bounds = scaledPoly.getBounds();
        Rectangle resultBounds = resultPoly.getBounds();

        Assertions.assertTrue(bounds.width == resultBounds.width);
        Assertions.assertTrue(bounds.height == resultBounds.height);
    }

    @Test
    public void testMatrixInversion() {
        Matrix inversResult = new Matrix(
                -0.02063, 0.01806, 0.01394,
                0.00177, 0.01452, -0.00493,
                0.01563, -0.01562, 0);

        Matrix inversCalculated = mMatrix2.invers();

        System.out.println(inversResult.toString());
        System.out.println(inversCalculated.toString());
    }

    @Test
    public void testZoomToFit() {
        Rectangle worldRect = new Rectangle(47944531, 608091485, 234500, 213463);
        Rectangle windowRect = new Rectangle(0, 0, 640, 480);

        Matrix projectedMatrix = Matrix.zoomToFit(worldRect, windowRect);

        Rectangle projectedRect = projectedMatrix.multiply(worldRect);
        System.out.println(projectedRect);

        Rectangle projectedBack = projectedMatrix.invers().multiply(projectedRect);
        System.out.println(projectedBack);
    }
}
